from setuptools import setup

setup(
    name='kanban',
    packages=['kanban'],
    include_package_data=True,
    zip_safe=False,
    install_requires=[
        'flask>=1.0.2',
        'flask_sqlalchemy>=2.3.2',
        'argon2_cffi>=18.3.0',
        'pymysql>=0.9.3',
        'SQLAlchemy==1.3.23', # workaround for this, 1.4 breaks flask_sqlalchemy :
        # https://github.com/pallets/flask-sqlalchemy/issues/885
    ],
)
