# Kanban

![A screenshot of the Kanban app's edition screen](screenshot_edit_screen.png)

## Quick Setup

First, install pip and venv. On Debian or Ubuntu, this is done with the following commands :

```
apt install python3-venv python3-pip
```

Then, create a virtual environment : 

```
python3 -m venv venv
. venv/bin/activate
pip3 install -e .
```

After that, initialize the application with an empty SQLite database.
Press enter to select the default options.

```
$ FLASK_APP=kanban flask init
Enter a DB engine (sqlite, postgresql, mysql, oracle, mssql, other) [sqlite]: 
Enter the database path [[install_path]/kanban/instance/kanban_data.sqlite]: 
The app is now ready.
```

Optional : Fill the database with randomly generated data.
This will generate 26 users named "user_[a-z]" with "password" as their password,
along with 26 boards with random notes.

```
FLASK_APP=kanban flask init_example_db
```

Now that we're done, launch the application server with this command :

```
FLASK_APP=kanban flask run
```

Please note that the interface is in French.


## Context

(TODO)
