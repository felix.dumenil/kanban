import flask
import kanban

from common_tests import *


class TestSignup:
    def test_signup_already_connected(self, client, user):
        resp = client.get('/signup')
        assert resp.status_code == 302

    def test_signup(self, client):
        resp = client.get('/signup')
        assert resp.status_code == 200


class TestLogout:
    def test_logout(self, client, user):
        resp = client.get('/logout')
        assert resp.status_code == 302
        assert 'user_id' not in flask.session

    def test_logout_already_disconnected(self, client):
        resp = client.get('/logout')
        assert resp.status_code == 302
        assert 'user_id' not in flask.session


class TestDoSignup:
    def test_do_signup_not_enough_params(self, client):
        resp = client.post('/post/signup', data={
            'login': 'flask_test',
            'display_name': 'Flask Test',
            'password': '12345',
        })
        assert resp.status_code == 302
        assert kanban.model.User.query.count() == 0

        resp = client.post('/post/signup', data={
            'login': 'flask_test',
            'password': '12345',
            'password_redundancy': '12345',
        })
        assert resp.status_code == 302
        assert kanban.model.User.query.count() == 0

        resp = client.post('/post/signup', data={
            'login': 'flask_test',
            'display_name': 'Flask Test',
            'password_redundancy': '12345',
        })
        assert resp.status_code == 302
        assert kanban.model.User.query.count() == 0

        resp = client.post('/post/signup', data={
            'display_name': 'Flask Test',
            'password': '12345',
            'password_redundancy': '12345',
        })
        assert resp.status_code == 302
        assert kanban.model.User.query.count() == 0

        # empty fields

        resp = client.post('/post/signup', data={
            'login': '',
            'display_name': 'Flask Test',
            'password': '12345',
            'password_redundancy': '12345',
        })
        assert resp.status_code == 302
        assert kanban.model.User.query.count() == 0

        resp = client.post('/post/signup', data={
            'login': 'flask_test',
            'display_name': '',
            'password': '12345',
            'password_redundancy': '12345',
        })
        assert resp.status_code == 302
        assert kanban.model.User.query.count() == 0

        resp = client.post('/post/signup', data={
            'login': 'flask_test',
            'display_name': 'Flask Test',
            'password': '',
            'password_redundancy': '',
        })
        assert resp.status_code == 302
        assert kanban.model.User.query.count() == 0

    def test_do_signup(self, client):
        resp = client.post('/post/signup', data={
            'login': 'flask_test',
            'display_name': 'Flask Test',
            'password': '12345',
            'password_redundancy': '12345',
        })
        assert resp.status_code == 302

        all_users = kanban.model.User.query.all()
        assert len(all_users) == 1
        assert all_users[0].login == 'flask_test'

        assert flask.session['user_id'] == all_users[0].id

        # user already logged in

        resp = client.post('/post/signup', data={
            'login': 'flask_test2',
            'display_name': 'Flask Test2',
            'password': '45678',
            'password_redundancy': '45678',
        })
        assert resp.status_code == 302

        count_users = kanban.model.User.query.count()
        assert count_users == 1

        # let's log out

        resp = client.get('/logout')
        assert resp.status_code == 302
        assert 'user_id' not in flask.session

        # user already exist

        resp = client.post('/post/signup', data={
            'login': 'flask_test',
            'display_name': 'Flask Test',
            'password': '45678',
            'password_redundancy': '45678',
        })
        assert resp.status_code == 302

        count_users = kanban.model.User.query.count()
        assert count_users == 1
        assert 'user_id' not in flask.session

        # password_redundancy != password

        resp = client.post('/post/signup', data={
            'login': 'flask_test2',
            'display_name': 'Flask Test2',
            'password': '12345',
            'password_redundancy': '45678',
        })
        assert resp.status_code == 302

        count_users = kanban.model.User.query.count()
        assert count_users == 1
        assert 'user_id' not in flask.session

        # OK: password already exist

        resp = client.post('/post/signup', data={
            'login': 'flask_test3',
            'display_name': 'Flask Test3',
            'password': '12345',
            'password_redundancy': '12345',
        })

        count_users = kanban.model.User.query.count()
        assert count_users == 2

        user2 = kanban.model.User.query.filter_by(login='flask_test3').one()
        assert user2.login == 'flask_test3'

        assert flask.session['user_id'] == user2.id


class TestLogin:
    def test_login(self, client, user):
        # we start logged in, let's log out

        resp = client.get('/logout')
        assert resp.status_code == 302
        assert 'user_id' not in flask.session

        # incorrect request

        resp = client.post('/post/connect', data={
            'login': 'flask_test',
        })
        assert resp.status_code == 302
        assert 'user_id' not in flask.session

        # wrong password

        resp = client.post('/post/connect', data={
            'login': 'flask_test',
            'password': '45678',
        })
        assert resp.status_code == 302
        assert 'user_id' not in flask.session

        # wrong login

        resp = client.post('/post/connect', data={
            'login': 'flask_test2',
            'password': '12345',
        })
        assert resp.status_code == 302
        assert 'user_id' not in flask.session

        # ok

        resp = client.post('/post/connect', data={
            'login': 'flask_test',
            'password': '12345',
        })
        assert resp.status_code == 302
        assert flask.session['user_id'] == user.id

        # already connected

        resp = client.post('/post/connect', data={
            'login': 'flask_test',
            'password': '12345',
        })
        assert resp.status_code == 302
        assert flask.session['user_id'] == user.id
