import kanban

from common_tests import *


class TestUserSearch:
    def test_search_user_names(self, client, example_db):
        # Must be connected

        resp = client.get('/get/search_user_names?q=user')
        assert resp.status_code == 401

        example_db['connect_user_member'](client)

        # Must have query

        resp = client.get('/get/search_user_names?q=')
        assert resp.status_code == 400

        resp = client.get('/get/search_user_names')
        assert resp.status_code == 400

        # Correct

        resp = client.get('/get/search_user_names?q=user')
        assert resp.status_code == 200
        assert resp.json['status'] == 'ok'

        assert len(resp.json['result']) == 3
        assert [example_db['user_owner'], 'user_owner'] in resp.json['result']
        assert [example_db['user_member'], 'user_member'] in resp.json['result']
        assert [example_db['user_other'], 'user_other'] in resp.json['result']

        resp = client.get('/get/search_user_names?q=user_o')
        assert resp.status_code == 200
        assert resp.json['status'] == 'ok'

        assert len(resp.json['result']) == 2
        assert [example_db['user_owner'], 'user_owner'] in resp.json['result']
        assert [example_db['user_other'], 'user_other'] in resp.json['result']

        # No results

        resp = client.get('/get/search_user_names?q=bill_gates')
        assert resp.status_code == 200
        assert resp.json['status'] == 'ok'

        assert len(resp.json['result']) == 0


class TestBoardCreation:
    def test_board_creation_must_login(self, client):
        resp = client.get('/board/create')
        assert resp.status_code == 302

    def test_board_creation(self, client, user):
        resp = client.get('/board/create')
        assert resp.status_code == 200


class TestBoardEdition:
    def test_board_edition_forbidden(self, client, example_db):
        # Not connected

        resp = client.get('/board/{}/settings'
                          .format(example_db['pub_board']))
        assert resp.status_code == 401

        resp = client.get('/board/{}/settings'
                          .format(example_db['priv_board']))
        assert resp.status_code == 401

        # Not member

        example_db['connect_user_other'](client)

        resp = client.get(example_db['pub_board_url'] + '/settings')
        assert resp.status_code == 403

        resp = client.get(example_db['priv_board_url'] + '/settings')
        assert resp.status_code == 403

        # Not owner

        example_db['connect_user_member'](client)

        resp = client.get(example_db['pub_board_url'] + '/settings')
        assert resp.status_code == 403

        resp = client.get(example_db['priv_board_url'] + '/settings')
        assert resp.status_code == 403

        # Not existing
        example_db['connect_user_owner'](client)

        resp = client.get('/board/{}/settings'
                          .format(example_db['pub_board'] + 500))
        assert resp.status_code == 404

    def test_board_edition(self, client, example_db):
        example_db['connect_user_owner'](client)

        resp = client.get(example_db['pub_board_url'] + '/settings')
        assert resp.status_code == 200

        resp = client.get(example_db['priv_board_url'] + '/settings')
        assert resp.status_code == 200


class TestBoardDoCreate:
    def test_board_do_create_board_must_login(self, client):
        resp = client.post('/post/board_settings')
        assert resp.status_code == 401
        assert kanban.model.Board.query.count() == 0

    def test_board_do_create_board_no_json(self, client, user):
        resp = client.post('/post/board_settings')
        assert resp.status_code == 400
        assert kanban.model.Board.query.count() == 0

    def test_kanban_do_create_board_bad_json(self, client, user):
        # no name
        resp = client.post('/post/board_settings', json={
            'board': {
                'id': None,
                'public': False,
                'columns': [
                    {'id': None, 'name': 'Stories', 'mandatory': True},
                    {'id': None, 'name': 'En cours', 'mandatory': False},
                    {'id': None, 'name': 'Terminées', 'mandatory': True},
                ],
                'deleted_columns': [],
                'subs': [{
                    'user_id': user.id,
                    'user_name': user.display_name,
                    'owner': True,
                    'brand_new': True,
                }],
                'deleted_subs': [],
            }
        })
        assert resp.status_code == 400
        assert kanban.model.Board.query.count() == 0

        # no public
        resp = client.post('/post/board_settings', json={
            'board': {
                'id': None,
                'name': 'mon_tableau',
                'columns': [
                    {'id': None, 'name': 'Stories', 'mandatory': True},
                    {'id': None, 'name': 'En cours', 'mandatory': False},
                    {'id': None, 'name': 'Terminées', 'mandatory': True},
                ],
                'deleted_columns': [],
                'subs': [{
                    'user_id': user.id,
                    'user_name': user.display_name,
                    'owner': True,
                    'brand_new': True,
                }],
                'deleted_subs': [],
            }
        })
        assert resp.status_code == 400
        assert kanban.model.Board.query.count() == 0

        # no column
        resp = client.post('/post/board_settings', json={
            'board': {
                'id': None,
                'name': 'mon_tableau',
                'public': False,
                'deleted_columns': [],
                'subs': [{
                    'user_id': user.id,
                    'user_name': user.display_name,
                    'owner': True,
                    'brand_new': True,
                }],
                'deleted_subs': [],
            }
        })
        assert resp.status_code == 400
        assert kanban.model.Board.query.count() == 0

        # no column.name
        resp = client.post('/post/board_settings', json={
            'board': {
                'id': None,
                'name': 'mon_tableau',
                'public': False,
                'columns': [
                    {'id': None, 'mandatory': True},
                    {'id': None, 'name': 'En cours', 'mandatory': False},
                    {'id': None, 'name': 'Terminées', 'mandatory': True},
                ],
                'deleted_columns': [],
                'subs': [{
                    'user_id': user.id,
                    'user_name': user.display_name,
                    'owner': True,
                    'brand_new': True,
                }],
                'deleted_subs': [],
            }
        })
        assert resp.status_code == 400
        assert kanban.model.Board.query.count() == 0

        # no column.mandatory
        resp = client.post('/post/board_settings', json={
            'board': {
                'id': None,
                'name': 'mon_tableau',
                'public': False,
                'columns': [
                    {'id': None, 'name': 'Stories'},
                    {'id': None, 'name': 'En cours', 'mandatory': False},
                    {'id': None, 'name': 'Terminées', 'mandatory': True},
                ],
                'deleted_columns': [],
                'subs': [{
                    'user_id': user.id,
                    'user_name': user.display_name,
                    'owner': True,
                    'brand_new': True,
                }],
                'deleted_subs': [],
            }
        })
        assert resp.status_code == 400
        assert kanban.model.Board.query.count() == 0

    def test_kanban_do_create_board_incorrect(self, client, user):
        # no owners
        resp = client.post('/post/board_settings', json={
            'board': {
                'id': None,
                'name': 'mon_tableau',
                'public': False,
                'columns': [
                    {'id': None, 'name': 'Stories', 'mandatory': True},
                    {'id': None, 'name': 'En cours', 'mandatory': False},
                    {'id': None, 'name': 'Terminées', 'mandatory': True},
                ],
                'deleted_columns': [],
                'subs': [{
                    'user_id': user.id,
                    'user_name': user.display_name,
                    'owner': False,
                    'brand_new': True,
                }],
                'deleted_subs': [],
            }
        })
        assert resp.status_code == 400
        assert kanban.model.Board.query.count() == 0

    def test_kanban_do_create_board(self, client, user):
        resp = client.post('/post/board_settings', json={
            'board': {
                'id': None,
                'name': 'mon_tableau',
                'public': True,
                'columns': [
                    {'id': None, 'name': 'Stories', 'mandatory': True},
                    {'id': None, 'name': 'En cours', 'mandatory': False},
                    {'id': None, 'name': 'Terminées', 'mandatory': True},
                ],
                'deleted_columns': [],
                'subs': [{
                    'user_id': user.id,
                    'user_name': user.display_name,
                    'owner': True,
                    'brand_new': True,
                }],
                'deleted_subs': [],
            }
        })
        assert resp.status_code == 200
        assert kanban.model.Board.query.count() == 1

        mon_tableau = kanban.model.Board.query.one()
        assert mon_tableau.name == 'mon_tableau'
        assert len(mon_tableau.columns) == 3
        assert mon_tableau.public is True
        assert mon_tableau.columns[0].name == 'Stories'
        assert mon_tableau.columns[0].mandatory is True
        assert mon_tableau.columns[1].name == 'En cours'
        assert mon_tableau.columns[1].mandatory is False
        assert mon_tableau.columns[2].name == 'Terminées'
        assert mon_tableau.columns[2].mandatory is True

    def test_kanban_do_edit_board(self, client, example_db):
        example_db['connect_user_owner'](client)

        resp = client.post('/post/board_settings', json={
            'board': {
                'id': example_db['pub_board'],
                'name': 'mon_tableau',
                'public': False,
                # Delete, add and change a column
                'columns': [
                    {
                        'id': example_db['pub_columns'][0],
                        'name': 'Cool Stories',
                        'mandatory': True,
                    }, {
                        'id': None,
                        'name': 'New column',
                        'mandatory': False,
                    }, {
                        'id': example_db['pub_columns'][2],
                        'name': 'Terminées',
                        'mandatory': True
                    },
                ],
                'deleted_columns': [
                    {
                        'id': example_db['pub_columns'][1],
                        'name': 'En cours',
                        'mandatory': False,
                    }
                ],
                # Delete, add and change a sub
                'subs': [{
                    'user_id': example_db['user_member'],
                    'user_name': 'user_member',
                    'owner': True,
                    'brand_new': False,
                }, {
                    'user_id': example_db['user_other'],
                    'user_name': 'user_other',
                    'owner': False,
                    'brand_new': True,
                }],
                'deleted_subs': [{
                    'user_id': example_db['user_owner'],
                    'user_name': 'user_owner',
                    'owner': True,
                    'brand_new': False,
                }],
            }
        })
        assert resp.status_code == 200
        assert kanban.model.Board.query.count() == 2
        assert kanban.model.Column.query.count() == 6
        assert kanban.model.Subscription.query.count() == 4

        board = kanban.model.Board.query.get(example_db['pub_board'])
        assert board.name == 'mon_tableau'
        assert not board.public

        assert len(board.columns) == 3
        for column in board.columns:
            assert column.id != example_db['pub_columns'][1]
            if column.id == example_db['pub_columns'][0]:
                assert column.name == 'Cool Stories'
                assert column.mandatory
                assert len(column.tasks) == 3
            elif column.id == example_db['pub_columns'][2]:
                assert column.name == 'Terminées'
                assert column.mandatory
                assert len(column.tasks) == 0
            else:  # the new column
                assert column.name == 'New column'
                assert not column.mandatory
                assert len(column.tasks) == 0

        assert len(board.subs) == 2
        for sub in board.subs:
            assert sub.user_id != example_db['user_owner']
            if sub.user_id == example_db['user_member']:
                assert sub.owner
            elif sub.user_id == example_db['user_other']:
                assert not sub.owner
            else:
                assert False
