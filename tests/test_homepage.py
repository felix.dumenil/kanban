from common_tests import *


class TestHomepage:
    def test_homepage_anonymous(self, client, example_db):
        resp = client.get('/')
        assert resp.status_code == 200

    def test_homepage_logged_in(self, client, user, example_db):
        resp = client.get('/')
        assert resp.status_code == 200
