import kanban

from common_tests import *


def test_settings_page_logged_out(app, client):
    resp = client.get('/my_account')
    assert resp.status_code == 401


def test_settings_page(app, client, user):
    resp = client.get('/my_account')
    assert resp.status_code == 200


def test_change_profile_logged_out(app, client):
    resp = client.post('/post/change_profile', data={
        'display_name': 'new display name',
    })
    assert resp.status_code == 401


def test_change_profile(app, client, user):
    # no display_name
    resp = client.post('/post/change_profile')
    assert resp.status_code == 302

    # correct
    resp = client.post('/post/change_profile', data={
        'display_name': 'new display name',
    })
    assert resp.status_code == 302

    user_new = kanban.model.User.query.filter_by(login='flask_test').one()

    assert user_new.display_name == 'new display name'


def test_change_password_logged_out(app, client):
    resp = client.post('/post/change_password', data={
        'old_password': '12345',
        'new_password': '67890',
        'new_password_redundancy': '67890',
    })
    assert resp.status_code == 401


def test_change_password(app, client, user):
    def get_user():
        return kanban.model.User.query.filter_by(login='flask_test').one()

    old_hash = user.password_hash

    # no old password
    resp = client.post('/post/change_password', data={
        'new_password': '67890',
        'new_password_redundancy': '67890',
    })
    assert resp.status_code == 302

    assert get_user().password_hash == old_hash

    # no new password
    resp = client.post('/post/change_password', data={
        'old_password': '12345',
        'new_password_redundancy': '67890',
    })
    assert resp.status_code == 302

    assert get_user().password_hash == old_hash

    # no 2nd new password
    resp = client.post('/post/change_password', data={
        'old_password': '12345',
        'new_password': '67890',
    })
    assert resp.status_code == 302

    assert get_user().password_hash == old_hash

    # wrong old password
    resp = client.post('/post/change_password', data={
        'old_password': 'abcde',
        'new_password': '67890',
        'new_password_redundancy': '67890',
    })
    assert resp.status_code == 302

    assert get_user().password_hash == old_hash

    # different new passwords
    resp = client.post('/post/change_password', data={
        'old_password': '12345',
        'new_password': '67890',
        'new_password_redundancy': 'abcde',
    })
    assert resp.status_code == 302

    assert get_user().password_hash == old_hash

    # correct
    resp = client.post('/post/change_password', data={
        'old_password': '12345',
        'new_password': '67890',
        'new_password_redundancy': '67890',
    })
    assert resp.status_code == 302

    user_new = get_user()

    assert user_new.password_hash != old_hash
    assert user_new.is_password_valid('67890')


def test_delete_account_logged_out(app, client):
    resp = client.post('/post/delete_account')
    assert resp.status_code == 401


def test_delete_account(app, client, user):
    resp = client.post('/post/delete_account')
    assert resp.status_code == 302

    user_new = (
        kanban.model.User.query
        .filter_by(login='flask_test')
        .one_or_none()
    )

    assert user_new is None


def test_delete_account_member_with_assigned_tasks(app, client, example_db):
    example_db['connect_user_member'](client)

    resp = client.post('/post/delete_account')
    assert resp.status_code == 302

    user_member = kanban.model.User.query.get(example_db['user_member'])
    assert user_member is None

    # Have the tasks been unassigned ?
    pub_task_member = kanban.model.Task.query.get(
        example_db['pub_task_member'])
    assert pub_task_member.assigned_user_id is None

    priv_task_member = kanban.model.Task.query.get(
        example_db['priv_task_member'])
    assert priv_task_member.assigned_user_id is None

    # Are the subscriptions deleted ?
    pub_board = kanban.model.Board.query.get(example_db['pub_board'])
    assert len(pub_board.subs) == 1
    assert pub_board.subs[0].user_id == example_db['user_owner']

    priv_board = kanban.model.Board.query.get(example_db['priv_board'])
    assert len(priv_board.subs) == 1
    assert pub_board.subs[0].user_id == example_db['user_owner']


def test_delete_account_owner_with_assigned_tasks(app, client, example_db):
    example_db['connect_user_owner'](client)

    resp = client.post('/post/delete_account')
    assert resp.status_code == 302

    # If the user is the owner of a board with another member, but no other
    # owner, they shouldn't be able to delete their account.

    user_owner = kanban.model.User.query.get(example_db['user_owner'])
    assert user_owner is not None

    resp = client.post('/post/board_settings', json={
        'board': {
            'id': example_db['pub_board'],
            'name': 'pub_board',
            'public': True,
            'columns': [
                {
                    'id': example_db['pub_columns'][0],
                    'name': 'Cool Stories',
                    'mandatory': True,
                }, {
                    'id': example_db['pub_columns'][1],
                    'name': 'En cours',
                    'mandatory': False,
                }, {
                    'id': example_db['pub_columns'][2],
                    'name': 'Terminées',
                    'mandatory': True
                },
            ],
            'deleted_columns': [
            ],
            'subs': [{
                'user_id': example_db['user_owner'],
                'user_name': 'user_owner',
                'owner': True,
                'brand_new': False,
            }, {
                'user_id': example_db['user_member'],
                'user_name': 'user_member',
                'owner': True,  # Promote other member to owner
                'brand_new': False,
            }],
            'deleted_subs': [],
        }
    })
    assert resp.status_code == 200

    resp = client.post('/post/board_settings', json={
        'board': {
            'id': example_db['priv_board'],
            'name': 'priv_board',
            'public': True,
            'columns': [
                {
                    'id': example_db['priv_columns'][0],
                    'name': 'Cool Stories',
                    'mandatory': True,
                }, {
                    'id': example_db['priv_columns'][1],
                    'name': 'En cours',
                    'mandatory': False,
                }, {
                    'id': example_db['priv_columns'][2],
                    'name': 'Terminées',
                    'mandatory': True
                },
            ],
            'deleted_columns': [
            ],
            'subs': [{
                'user_id': example_db['user_owner'],
                'user_name': 'user_owner',
                'owner': True,
                'brand_new': False,
            }],
            'deleted_subs': [{  # Remove other member
                'user_id': example_db['user_member'],
                'user_name': 'user_member',
                'owner': False,
                'brand_new': False,
            }],
        }
    })
    assert resp.status_code == 200

    resp = client.post('/post/delete_account')
    assert resp.status_code == 302

    # Now, this should work

    user_owner = kanban.model.User.query.get(example_db['user_owner'])
    assert user_owner is None

    pub_board = kanban.model.Board.query.get(example_db['pub_board'])
    assert pub_board is not None

    priv_board = kanban.model.Board.query.get(example_db['priv_board'])
    assert priv_board is None
