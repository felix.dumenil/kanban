import pytest
import kanban


@pytest.fixture
def app():
    app = kanban.create_app(is_testing=True)

    kanban.init_db(app)

    with app.app_context():
        yield app


@pytest.fixture
def client(app):
    client = app.test_client()
    with client:
        yield client


@pytest.fixture
def user(client):
    client.post('/post/signup', data={
        'login': 'flask_test',
        'display_name': 'Flask Test',
        'password': '12345',
        'password_redundancy': '12345',
    })

    return kanban.model.User.query.filter_by(login='flask_test').one()


@pytest.fixture
def board_created_id(client, user):
    client.post('/post/board_settings', json={
        'board': {
            'id': None,
            'name': 'mon_tableau',
            'public': False,
            'columns': [
                {'id': None, 'name': 'Stories', 'mandatory': True},
                {'id': None, 'name': 'En cours', 'mandatory': False},
                {'id': None, 'name': 'Terminées', 'mandatory': True},
            ],
            'deleted_columns': [],
            'subs': [{
                'user_id': user.id,
                'user_name': user.display_name,
                'owner': True,
                'brand_new': True,
            }],
            'deleted_subs': [],
        }
    })

    mon_tableau = kanban.model.Board.query.filter_by(name='mon_tableau').one()
    return mon_tableau.id


@pytest.fixture
def example_db(app):
    # Users

    def new_user(name):
        return kanban.model.User.create(name, name, 'password')

    user_owner = new_user('user_owner')
    kanban.db.session.add(user_owner)

    user_member = new_user('user_member')
    kanban.db.session.add(user_member)

    user_other = new_user('user_other')
    kanban.db.session.add(user_other)

    # Boards

    pub_board = kanban.model.Board(name='Public Board', public=True)
    kanban.db.session.add(pub_board)

    priv_board = kanban.model.Board(name='Private Board', public=False)
    kanban.db.session.add(priv_board)

    # Subs

    kanban.db.session.add(kanban.model.Subscription(
        user=user_owner, board=pub_board, owner=True))
    kanban.db.session.add(kanban.model.Subscription(
        user=user_owner, board=priv_board, owner=True))
    kanban.db.session.add(kanban.model.Subscription(
        user=user_member, board=pub_board, owner=False))
    kanban.db.session.add(kanban.model.Subscription(
        user=user_member, board=priv_board, owner=False))

    # Columns

    pub_columns = [
        kanban.model.Column(
            name="Stories", mandatory=True, board=pub_board, order=0),
        kanban.model.Column(
            name="En cours", mandatory=False, board=pub_board, order=1),
        kanban.model.Column(
            name="Terminées", mandatory=True, board=pub_board, order=2)
    ]
    kanban.db.session.add(pub_columns[0])
    kanban.db.session.add(pub_columns[1])
    kanban.db.session.add(pub_columns[2])

    priv_columns = [
        kanban.model.Column(
            name="Stories", mandatory=True, board=priv_board, order=0),
        kanban.model.Column(
            name="En cours", mandatory=False, board=priv_board, order=1),
        kanban.model.Column(
            name="Terminées", mandatory=True, board=priv_board, order=2)
    ]
    kanban.db.session.add(priv_columns[0])
    kanban.db.session.add(priv_columns[1])
    kanban.db.session.add(priv_columns[2])

    # Tasks

    pub_task_owner = kanban.model.Task(
        column=pub_columns[0],
        description='pub_task_owner',
        color=kanban.model.TaskColor.red,
        assigned_user=user_owner,
    )
    kanban.db.session.add(pub_task_owner)

    pub_task_member = kanban.model.Task(
        column=pub_columns[0],
        description='pub_task_member',
        color=kanban.model.TaskColor.green,
        assigned_user=user_member,
    )
    kanban.db.session.add(pub_task_member)

    pub_task_none = kanban.model.Task(
        column=pub_columns[0],
        description='pub_task_none',
        color=kanban.model.TaskColor.blue,
        assigned_user=None,
    )
    kanban.db.session.add(pub_task_none)

    priv_task_owner = kanban.model.Task(
        column=priv_columns[0],
        description='priv_task_owner',
        color=kanban.model.TaskColor.red,
        assigned_user=user_owner,
    )
    kanban.db.session.add(priv_task_owner)

    priv_task_member = kanban.model.Task(
        column=priv_columns[0],
        description='priv_task_member',
        color=kanban.model.TaskColor.green,
        assigned_user=user_member,
    )
    kanban.db.session.add(priv_task_member)

    priv_task_none = kanban.model.Task(
        column=priv_columns[0],
        description='priv_task_none',
        color=kanban.model.TaskColor.blue,
        assigned_user=None,
    )
    kanban.db.session.add(priv_task_none)

    kanban.db.session.commit()

    def connect_user_owner(client):
        client.post('/post/connect',
                    data={'login': 'user_owner', 'password': 'password'})

    def connect_user_member(client):
        client.post('/post/connect',
                    data={'login': 'user_member', 'password': 'password'})

    def connect_user_other(client):
        client.post('/post/connect',
                    data={'login': 'user_other', 'password': 'password'})

    with app.test_request_context('/'):
        pub_board_url = pub_board.get_url()
        priv_board_url = priv_board.get_url()

    return {
        'user_owner': user_owner.id,
        'user_member': user_member.id,
        'user_other': user_other.id,

        'pub_board': pub_board.id,
        'priv_board': priv_board.id,

        'pub_board_url': pub_board_url,
        'priv_board_url': priv_board_url,

        'pub_columns': [col.id for col in pub_columns],
        'priv_columns': [col.id for col in priv_columns],

        'pub_task_owner': pub_task_owner.id,
        'pub_task_member': pub_task_member.id,
        'pub_task_none': pub_task_none.id,

        'priv_task_owner': priv_task_owner.id,
        'priv_task_member': priv_task_member.id,
        'priv_task_none': priv_task_none.id,

        'connect_user_owner': connect_user_owner,
        'connect_user_member': connect_user_member,
        'connect_user_other': connect_user_other,
    }
