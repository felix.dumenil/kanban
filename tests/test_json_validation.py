from kanban.json_validation import validate_json


def test_validate_json_simple():
    model = {
        'str': 'str',
        'int': 'int',
        'float': 'float',
        'bool': 'bool',
        'null': 'null',
    }
    assert(validate_json({
        'str': 'blahblah',
        'int': 42,
        'float': 42.5,
        'bool': False,
        'null': None,
    }, model))
    assert(not validate_json('blahblah', model))
    assert(not validate_json(42, model))
    assert(not validate_json(42.5, model))
    assert(not validate_json(False, model))
    assert(not validate_json(None, model))
    assert(not validate_json({
        'str': 'blahblah',
        'int': 42,
        'float': 42.5,
        'bool': False,
        'null': None,
        'too_much': True,
    }, model))


def test_validate_json_union():
    model = ('str', 'int', 'null')
    assert(validate_json('blahblah', model))
    assert(validate_json(42, model))
    assert(validate_json(None, model))
    assert(not validate_json(42.5, model))
    assert(not validate_json(False, model))


def test_validate_json_array():
    model = ['int']
    assert(validate_json([1, 2, 3], model))

    model = [('str', 'int')]
    assert(validate_json([1, 2, 'C', 'D', 3], model))
    assert(not validate_json([1, 2.5, 'C', 'D', 3], model))


def test_validate_json_array_of_objects():
    model = [{'id': 'int', 'name': 'str'}]
    assert(validate_json([
        {'id': 32, 'name': 'Jean Dupont'},
        {'id': 98, 'name': 'Jeanne Dubois'},
        {'id': -50, 'name': 'Gregory'},
    ], model))
    assert(not validate_json([
        {'id': 32},
        {'id': 98, 'name': 'Jeanne Dubois'},
        {'name': 'Gregory'},
    ], model))


def test_validate_json_enums():
    model = ('val:red', 'val:green', 'val:blue')
    assert(validate_json('red', model))
    assert(validate_json('green', model))
    assert(validate_json('blue', model))
    assert(not validate_json('black', model))
    assert(not validate_json(None, model))
