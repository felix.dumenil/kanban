import kanban
import datetime

from common_tests import *


class TestBoardView:
    def test_board_view_inexistant(self, client, user):
        resp = client.get('/board/42')
        assert resp.status_code == 404

    def test_board_view_owner(self, client, example_db):
        example_db['connect_user_owner'](client)

        resp = client.get(example_db['pub_board_url'])
        assert resp.status_code == 200

        resp = client.get(example_db['priv_board_url'])
        assert resp.status_code == 200

    def test_board_view_member(self, client, example_db):
        example_db['connect_user_member'](client)

        resp = client.get(example_db['pub_board_url'])
        assert resp.status_code == 200

        resp = client.get(example_db['priv_board_url'])
        assert resp.status_code == 200

    def test_board_view_other(self, client, example_db):
        example_db['connect_user_other'](client)

        resp = client.get(example_db['pub_board_url'])
        assert resp.status_code == 200

        resp = client.get(example_db['priv_board_url'])
        assert resp.status_code == 403

    def test_board_view_guest(self, client, example_db):
        resp = client.get(example_db['pub_board_url'])
        assert resp.status_code == 200

        resp = client.get(example_db['priv_board_url'])
        assert resp.status_code == 403


def test_do_edit_task(client, user, board_created_id):
    # no args
    resp = client.post('/post/edit_task')
    assert resp.status_code == 400
    assert kanban.model.Task.query.count() == 0

    # bad color
    col_id = kanban.model.Board.query.get(board_created_id).columns[1].id

    resp = client.post('/post/edit_task', json={
        'task': {
            'column_id': col_id,
            'id': None,
            'description': 'Boucler le projet',
            'color': 'rebeccapurple',
            'assigned_user_id': None,
            'due_date': None,
        }
    })
    assert resp.status_code == 400
    assert resp.get_json() == {'msg': 'Requête incomplète', 'status': 'err'}
    assert kanban.model.Task.query.count() == 0

    # unknown column

    resp = client.post('/post/edit_task', json={
        'task': {
            'column_id': col_id+500,
            'id': None,
            'description': 'Boucler le projet',
            'color': 'red',
            'assigned_user_id': None,
            'due_date': None,
        }
    })
    assert resp.status_code == 400
    assert resp.get_json() == {
        'msg': "Cette colonne n'existe pas",
        'status': 'err',
    }
    assert kanban.model.Task.query.count() == 0

    # bad date

    resp = client.post('/post/edit_task', json={
        'task': {
            'column_id': col_id,
            'id': None,
            'description': 'Boucler le projet',
            'color': 'red',
            'assigned_user_id': None,
            'due_date': '20/01/2018',
        }
    })
    assert resp.status_code == 400
    assert resp.get_json() == {
        'msg': "Date invalide",
        'status': 'err',
    }
    assert kanban.model.Task.query.count() == 0

    # correct

    resp = client.post('/post/edit_task', json={
        'task': {
            'column_id': col_id,
            'id': None,
            'description': 'Boucler le projet',
            'color': 'red',
            'assigned_user_id': None,
            'due_date': '2018-01-20',
        }
    })
    assert resp.status_code == 200
    assert resp.get_json() == {
        'task_id': 1,
        'status': 'ok',
    }
    assert kanban.model.Task.query.count() == 1

    board_created = kanban.model.Board.query.get(board_created_id)

    assert len(board_created.columns[1].tasks) == 1
    task = board_created.columns[1].tasks[0]
    assert task.description == 'Boucler le projet'
    assert task.color == kanban.model.TaskColor.red
    assert task.assigned_user_id is None
    assert task.due_date == datetime.date(2018, 1, 20)

    col2_id = board_created.columns[2].id

    # correct edit

    resp = client.post('/post/edit_task', json={
        'task': {
            'column_id': col2_id,
            'id': task.id,
            'description': 'Boucler le projet plus vite !',
            'color': 'green',
            'assigned_user_id': None,
            'due_date': None,
        }
    })
    assert resp.status_code == 200
    assert resp.get_json() == {
        'task_id': task.id,
        'status': 'ok',
    }
    assert kanban.model.Task.query.count() == 1

    board_created = kanban.model.Board.query.get(board_created_id)
    task = board_created.columns[2].tasks[0]
    assert task.description == 'Boucler le projet plus vite !'
    assert task.color == kanban.model.TaskColor.green
    assert task.assigned_user_id is None
    assert task.due_date is None


def test_assign_task(client, example_db):
    example_db['connect_user_member'](client)

    # unassign a task assigned to someone else : bad
    resp = client.post('/post/edit_task', json={
        'task': {
            'id': example_db['priv_task_owner'],
            'column_id': example_db['priv_columns'][0],
            'description': 'priv_task_owner',
            'color': 'red',
            'assigned_user_id': None,
            'due_date': None,
        }
    })
    print(resp.get_json())
    assert resp.status_code == 403
    assert resp.get_json() == {
        'msg': 'Vous ne pouvez pas affecter cet utilisateur',
        'status': 'err',
    }

    # unassign a task assigned to self : ok
    resp = client.post('/post/edit_task', json={
        'task': {
            'id': example_db['priv_task_member'],
            'column_id': example_db['priv_columns'][0],
            'description': 'priv_task_member',
            'color': 'green',
            'assigned_user_id': None,
            'due_date': None,
        }
    })
    assert resp.status_code == 200
    assert resp.get_json() == {
        'task_id': example_db['priv_task_member'],
        'status': 'ok',
    }

    # assign a task to someone else : bad
    resp = client.post('/post/edit_task', json={
        'task': {
            'id': example_db['priv_task_member'],
            'column_id': example_db['priv_columns'][0],
            'description': 'priv_task_member',
            'color': 'green',
            'assigned_user_id': example_db['user_owner'],
            'due_date': None,
        }
    })
    assert resp.status_code == 403
    assert resp.get_json() == {
        'msg': 'Vous ne pouvez pas affecter cet utilisateur',
        'status': 'err',
    }

    # assign a task to self : ok
    resp = client.post('/post/edit_task', json={
        'task': {
            'id': example_db['priv_task_member'],
            'column_id': example_db['priv_columns'][0],
            'description': 'priv_task_member',
            'color': 'green',
            'assigned_user_id': example_db['user_member'],
            'due_date': None,
        }
    })
    assert resp.status_code == 200
    assert resp.get_json() == {
        'task_id': example_db['priv_task_member'],
        'status': 'ok',
    }


def test_assign_task_as_owner(client, example_db):
    example_db['connect_user_owner'](client)
    # unassign a task assigned to someone-else : ok
    resp = client.post('/post/edit_task', json={
        'task': {
            'id': example_db['priv_task_member'],
            'column_id': example_db['priv_columns'][0],
            'description': 'priv_task_member',
            'color': 'green',
            'assigned_user_id': None,
            'due_date': None,
        }
    })
    print(resp.get_json())
    assert resp.status_code == 200
    assert resp.get_json() == {
        'task_id': example_db['priv_task_member'],
        'status': 'ok',
    }

    # assign a task to someone-else : ok
    resp = client.post('/post/edit_task', json={
        'task': {
            'id': example_db['priv_task_owner'],
            'column_id': example_db['priv_columns'][0],
            'description': 'priv_task_owner',
            'color': 'green',
            'assigned_user_id': example_db['user_member'],
            'due_date': None,
        }
    })
    assert resp.status_code == 200
    assert resp.get_json() == {
        'task_id': example_db['priv_task_owner'],
        'status': 'ok',
    }


def test_do_delete_task(client, user, board_created_id):
    col_id = kanban.model.Board.query.get(board_created_id).columns[1].id
    resp = client.post('/post/edit_task', json={
        'task': {
            'column_id': col_id,
            'id': None,
            'description': 'Boucler le projet',
            'color': 'red',
            'assigned_user_id': None,
            'due_date': None,
        }
    })
    assert resp.status_code == 200
    assert kanban.model.Task.query.count() == 1

    task_id = kanban.model.Column.query.get(col_id).tasks[0].id

    # invalid request

    resp = client.post('/post/delete_task')
    assert resp.status_code == 400
    assert kanban.model.Task.query.count() == 1

    resp = client.post('/post/delete_task', json={})
    assert resp.status_code == 400
    assert kanban.model.Task.query.count() == 1

    # doesn't exist

    resp = client.post('/post/delete_task', json={'task_id': task_id+1})
    assert resp.status_code == 404
    assert kanban.model.Task.query.count() == 1

    # correct

    resp = client.post('/post/delete_task', json={'task_id': task_id})
    assert resp.status_code == 200
    assert kanban.model.Task.query.count() == 0


def test_do_delete_task_permissions_member(example_db, client):
    example_db['connect_user_member'](client)

    # a member can delete a task attributed to no-one

    resp = client.post('/post/delete_task',
                       json={'task_id': example_db['pub_task_none']})
    assert resp.status_code == 200
    assert kanban.model.Task.query.get(example_db['pub_task_none']) is None

    # a member can delete a task attributed to themselves

    resp = client.post('/post/delete_task',
                       json={'task_id': example_db['pub_task_member']})
    assert resp.status_code == 200
    assert kanban.model.Task.query.get(example_db['pub_task_member']) is None

    # a member CANNOT delete a task attributed to someone else

    resp = client.post('/post/delete_task',
                       json={'task_id': example_db['pub_task_owner']})
    assert resp.status_code == 403
    assert kanban.model.Task.query.get(example_db['pub_task_owner']) is not None


def test_do_delete_task_permissions_owner(example_db, client):
    example_db['connect_user_owner'](client)

    # an owner can delete a task attributed to no-one

    resp = client.post('/post/delete_task',
                       json={'task_id': example_db['pub_task_none']})
    assert resp.status_code == 200
    assert kanban.model.Task.query.get(example_db['pub_task_none']) is None

    # an owner can delete a task attributed to themselves

    resp = client.post('/post/delete_task',
                       json={'task_id': example_db['pub_task_member']})
    assert resp.status_code == 200
    assert kanban.model.Task.query.get(example_db['pub_task_member']) is None

    # an owner can delete a task attributed to someone else

    resp = client.post('/post/delete_task',
                       json={'task_id': example_db['pub_task_owner']})
    assert resp.status_code == 200
    assert kanban.model.Task.query.get(example_db['pub_task_owner']) is None


def test_do_delete_task_permissions_others(example_db, client):
    # a logged-out client cannot delete anything

    resp = client.post('/post/delete_task',
                       json={'task_id': example_db['pub_task_none']})
    assert resp.status_code == 403
    assert kanban.model.Task.query.get(example_db['pub_task_none']) is not None

    example_db['connect_user_other'](client)

    # a non-member cannot delete anything

    resp = client.post('/post/delete_task',
                       json={'task_id': example_db['pub_task_none']})
    assert resp.status_code == 403
    assert kanban.model.Task.query.get(example_db['pub_task_none']) is not None
