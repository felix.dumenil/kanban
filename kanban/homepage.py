from typing import List

import flask

from .auth import get_current_user
from . import model, apply_csp_with_nonce, init_config

bp = flask.Blueprint('homepage', __name__)


@bp.route('/')
def homepage():
    if not flask.current_app.config['KANBAN_INITIALIZED']:  # pragma: no cover
        return first_time_install()

    public_boards = model.Board.get_public_boards()
    user = get_current_user()
    if user is not None:
        return homepage_user(user, public_boards)
    else:
        return homepage_guest(public_boards)


def homepage_guest(public_boards: List[model.Board]):
    return flask.render_template(
        'homepage_guest.html',
        current_user=None,
        public_boards=public_boards,
    )


@apply_csp_with_nonce
def homepage_user(
        current_user: model.User,
        public_boards: List[model.Board],
        csp_nonce: str):

    assigned_tasks_sorted_by_board = (
        model.Task.query
        .filter_by(assigned_user=current_user)
        .join(model.Task.column)
        .order_by(model.Column.board_id)
        .all()
    )

    return flask.render_template(
        'homepage_user.html',
        current_user=current_user,
        public_boards=public_boards,
        csp_nonce=csp_nonce,
        assigned_tasks=assigned_tasks_sorted_by_board,
    )


def first_time_install():  # pragma: no cover
    return flask.render_template(
        'first_time_install.html',
        default_sqlite_path=(
            flask.current_app.instance_path + '/kanban_data.sqlite'),
    )


@bp.route('/post/init_app', methods=['POST'])
def create_app_config():  # pragma: no cover
    if flask.current_app.config['KANBAN_INITIALIZED']:
        raise Exception("App is already initialized !")

    db_uri = flask.request.form.get('db-uri', '')
    if db_uri == '':
        raise Exception("Wrong args !")

    init_config(flask.current_app, db_uri)

    # flask.flash("L'application a bien été initialisé. Félicitations !")
    return flask.redirect(flask.url_for('homepage.homepage'))
