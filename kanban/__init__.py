import os
import string, random, datetime
import functools

import flask
from flask_sqlalchemy import SQLAlchemy
import click
import argon2


db = SQLAlchemy()
ph = argon2.PasswordHasher()


def create_app(is_testing: bool=False):
    app = flask.Flask(__name__, instance_relative_config=True)

    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
    if app.env == 'development':
        # "style-src 'unsafe-inline'" is because without that,
        # I can't directly try CSS in the browser inspector.
        app.config['DEFAULT_CSP'] = (
            "default-src 'self'"
            + "; style-src 'self' 'unsafe-inline'")
    else:
        app.config['DEFAULT_CSP'] = "default-src 'self'"
    # app.config['SQLALCHEMY_ECHO'] = True

    app.config['KANBAN_INITIALIZED'] = False

    # This is just to remove the warnings about SQLALCHEMY_DATABASE_URI not
    # being set. We don't touch the database unless KANBAN_INITIALIZED is true
    app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///:memory:'

    app.config['TESTING'] = is_testing
    if is_testing:
        app.config['KANBAN_INITIALIZED'] = True
        app.secret_key = b'dev'
        app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///:memory:'
    else:  # pragma: no cover
        try:
            app.config.from_json('kanban_config.json', silent=True)
        except ValueError as e:
            print('Could not load json config :', repr(e))
        app.config.from_envvar('KANBAN_SETTINGS', silent=True)

    db.init_app(app)

    from . import model
    from . import homepage
    from . import board_settings
    from . import auth
    from . import board_view
    from . import account

    app.register_blueprint(homepage.bp)
    app.register_blueprint(board_settings.bp)
    app.register_blueprint(auth.bp)
    app.register_blueprint(board_view.bp)
    app.register_blueprint(account.bp)

    @app.errorhandler(404)
    def page_not_found(error):
        return flask.render_template(
            'error_page.html',
            current_user=auth.get_current_user(),
            err_msg="L'URL demandée n'a pas été trouvée.",
        ), 404

    @app.cli.command('init_db')
    def init_db_command():  # pragma: no cover
        init_db(app)
        click.echo('Initialized the database.')

    @app.cli.command('init_example_db')
    def init_example_db_command():  # pragma: no cover
        init_example_db(app, model)
        click.echo('Initialized the database with examples data.')

    @app.cli.command('init')
    def init_app_config_command():  # pragma: no cover
        db_engine = click.prompt(
            'Enter a DB engine',
            type=click.Choice([
                'sqlite',
                'postgresql',
                'mysql',
                'oracle',
                'mssql',
                'other',
            ]),
            default='sqlite',
        )
        if db_engine == 'sqlite':
            db_path = click.prompt(
                'Enter the database path',
                type=click.Path(
                    exists=False,
                    dir_okay=False,
                    readable=True,
                    writable=True,
                ),
                default=app.instance_path + '/kanban_data.sqlite',
            )
            db_uri = db_engine + ':///' + db_path
        elif db_engine == 'other':
            db_uri = click.prompt('Enter a SQLAlchemy URI')
        else:
            if db_engine == 'mysql':
                db_engine = 'mysql+pymysql'

            host = click.prompt('host(:port)')
            username = click.prompt('username', default='')
            password = click.prompt('password', default='', hide_input=True)
            db_name = click.prompt('database name', default='')
            db_uri = '{}://'.format(db_engine)
            if username != '':
                db_uri += username
                if password != '':
                    db_uri += ':' + password
                db_uri += '@'

            db_uri += host

            if db_name != '':
                db_uri += '/' + db_name

        init_config(app, db_uri)

        click.echo('The app is now ready.')

    @app.after_request
    def apply_csp(response):
        if response.headers.get('Content-Security-Policy', '') == '':
            response.headers['Content-Security-Policy'] = (
                app.config['DEFAULT_CSP'])
        return response

    return app


def apply_csp_with_nonce(func):
    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        csp_nonce = os.urandom(5).hex()
        kwargs['csp_nonce'] = csp_nonce

        resp = flask.make_response(func(*args, **kwargs))

        resp.headers['Content-Security-Policy'] = (
            flask.current_app.config['DEFAULT_CSP'] +
            "; script-src 'self' 'nonce-{}'".format(csp_nonce)
        )
        return resp

    return wrapper


def init_config(app: flask.Flask, db_uri: str):  # pragma: no cover
    config = {
        'KANBAN_INITIALIZED': True,
        'SQLALCHEMY_DATABASE_URI': db_uri,
        'SECRET_KEY': os.urandom(32).hex(),
    }

    app.config.from_mapping(config)

    os.makedirs(app.instance_path, exist_ok=True)

    init_db(app)

    with app.open_instance_resource('kanban_config.json', 'w') as f:
        flask.json.dump(config, f)


def init_db(app: flask.Flask):
    with app.app_context():
        db.create_all()


def init_example_db(app: flask.Flask, model):  # pragma: no cover
    def gen_word():
        result = ""
        for i in range(random.randint(3, 10)):
            result += random.choice(string.ascii_lowercase)
        return result

    def gen_description():
        result = ""
        for i in range(random.randint(3, 20)):
            if i > 0:
                result += ' '
            tag = random.choice([None, None, None, None, 'b', 'i', 'u', 's'])

            if tag is not None:
                result += '[' + tag + ']'

            result += gen_word()

            if tag is not None:
                result += '[/' + tag + ']'
        return result

    with app.app_context():
        print("Dropping...")
        db.drop_all()

        print("Creating tables...")
        db.create_all()

        print("Creating users...")
        users = []
        for char in string.ascii_uppercase:
            user = model.User.create(
                'user_' + char,
                'Utilisateur ' + char,
                'password',
            )
            db.session.add(user)
            users.append(user)

        print("Creating boards...")
        for char in string.ascii_uppercase:
            subs = [
                model.Subscription(
                    user=user,
                    owner=random.randint(0, 1) == 1,
                )
                for user in random.sample(users, 6)
            ]
            board = model.Board(
                name="Tableau " + char,
                public=random.randint(0, 4) == 1,
                subs=subs,
                columns=[
                    model.Column(
                        name=col[0],
                        mandatory=col[1],
                        order=i,
                        tasks=[
                            model.Task(
                                description=gen_description(),
                                color=random.choice(list(model.TaskColor)),
                                assigned_user=(
                                    None
                                    if random.randint(0, 2) == 0
                                    else random.choice(subs).user
                                ),
                                due_date=(
                                    None
                                    if random.randint(0, 2) == 0
                                    else datetime.date.today()
                                    + datetime.timedelta(
                                        days=random.randint(1, 15))
                                ),
                            )
                            for j in range(random.randint(0, 8))
                        ],
                    )
                    for i, col in enumerate([
                        ("Stories", True),
                        ("En cours", False),
                        ("Terminée", True),
                    ])
                ]
            )
            db.session.add(board)

        db.session.commit()
