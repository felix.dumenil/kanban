from typing import Optional


def validate_json(value, model) -> bool:
    return validate_json_with_desc(value, model) is None


def validate_json_with_desc(value, model) -> Optional[str]:
    """
        if model:
        - is a dict: value and model must have the same keys
          (unless model[key] == 'any'), and
          validate_json(value[key], model[key]) is True.
        - is a list: value must be a list,
          and all items of value must be validated by model[1].
        - is a tuple: value must be any of the types in model.
        - is 'str': value must be a string.
        - is 'float': value must be a float or a int (not a bool).
        - is 'int': value must be a int (not a bool).
        - is 'bool':  value must be a bool.
        - is 'null':  value must be None.
        - is 'any':  value can be anything.
    """
    if isinstance(model, dict):
        if not isinstance(value, dict):
            return '{!r} is not a dict ({!r})'.format(value, model)
        for key in model:
            if model[key] == 'any':
                continue
            if key not in value:
                return 'key {!r} not in {!r} ({!r})' \
                    .format(key, value, model)

            err = validate_json_with_desc(value[key], model[key])
            if err is not None:
                return err

        for key in value:
            if key not in model:
                return 'key {!r} should not be in in {!r}'.format(key, model)

    elif isinstance(model, list):
        if not isinstance(value, list):
            return '{!r} is not a list ({!r})'.format(value, model)
        for subval in value:
            err = validate_json_with_desc(subval, model[0])
            if err is not None:
                return err
        return None

    # a union
    elif isinstance(model, tuple):
        for possible_type in model:
            if validate_json(value, possible_type):
                return None
        return '{!r} is none of {!r}'.format(value, model)

    elif model == "str":
        if not isinstance(value, str):
            return '{!r} is not a string'.format(value)
    elif model == "int":
        if not (isinstance(value, int) and not isinstance(value, bool)):
            return '{!r} is not a int'.format(value)
    elif model == "float":
        if not ((isinstance(value, float) or isinstance(value, int))
                and not isinstance(value, bool)):
            return '{!r} is not a float'.format(value)
    elif model == "bool":
        if not isinstance(value, bool):
            return '{!r} is not a bool'.format(value)
    elif model == "null":
        if value is not None:
            return '{!r} is not null'.format(value)

    elif isinstance(model, str) and model.startswith("val:"):
        expected = model[len("val:"):]
        if value != expected:
            return '{!r} is not {!r}'.format(value, expected)

    elif model == "any":
        return None

    # (Yes, I know bool is a subtype of int)
    elif isinstance(model, int) or isinstance(model, bool):
        if value != model:
            return '{!r} is not {!r}'.format(value, model)

    else:
        raise Exception("invalid model : " + repr(model))

    return None
