from typing import Optional
import flask
from flask import request

from .auth import get_current_user
from . import model, db, apply_csp_with_nonce
from .json_validation import validate_json_with_desc

bp = flask.Blueprint('board_settings', __name__)


@bp.route('/board/create')
@apply_csp_with_nonce
def creation_page(csp_nonce: str):
    current_user = get_current_user()
    if current_user is None:
        flask.flash("Vous devez vous connecter avant de créer un tableau.",
                    "error")
        return flask.redirect(flask.url_for('homepage.homepage'))

    return flask.render_template(
        'board_settings.html',
        current_user=current_user,
        board=None,
        board_json=None,
        csp_nonce=csp_nonce,
    )


@bp.route('/board/<int:board_id>_<string:board_name>/settings')
@bp.route('/board/<int:board_id>/settings', defaults={'board_name': None})
@apply_csp_with_nonce
def edition_page(board_id: int, board_name: Optional[str], csp_nonce: str):
    current_user = get_current_user()

    def error(msg: str):
        return flask.render_template(
            'error_page.html',
            current_user=current_user,
            err_msg=msg,
        )
    if current_user is None:
        return (
            error("Vous devez vous connecter avant de modifier un tableau."),
            401,
        )

    board = model.Board.query.get(board_id)
    if board is None:
        return error("Ce tableau n'existe pas."), 404

    if not board.is_owned_by(current_user):
        return error("Vous n'avez pas le droit de modifier ce tableau."), 403

    if board.get_urlified_name() != board_name:
        return flask.redirect(board.get_settings_url())

    board_json = {
        'id': board.id,
        'name': board.name,
        'public': board.public,
        'columns': [{
            'id': col.id,
            'name': col.name,
            'mandatory': col.mandatory,
        } for col in board.columns],
        'deleted_columns': [],
        'subs': [{
            'user_id': sub.user_id,
            'user_name': sub.user.display_name,
            'owner': sub.owner,
            'brand_new': False,
        } for sub in board.subs],
        'deleted_subs': [],
    }

    return flask.render_template(
        'board_settings.html',
        current_user=current_user,
        board=board,
        board_json=board_json,
        csp_nonce=csp_nonce,
    )


@bp.route('/get/search_user_names')
def search_user_names():
    def error(msg: str):
        return flask.jsonify(status='err', msg=msg)

    current_user = get_current_user()
    if current_user is None:
        return error("Vous devez vous connecter."), 401

    query = request.args.get('q', '')
    if query == '':
        return error("Requête incomplète."), 400

    names = db.session.query(model.User.id, model.User.display_name) \
        .filter(model.User.display_name.contains(query)) \
        .limit(10) \
        .all()

    return flask.jsonify(status='ok', result=names)


@bp.route('/post/board_settings', methods=['POST'])
def do_create_board():
    def error(msg: str):
        flask.current_app.logger.warning(msg)
        return flask.jsonify(status='err', msg=msg)

    current_user = get_current_user()
    if current_user is None:
        return error(
            "Vous devez vous connecter avant de "
            + "créer/modifier un tableau."), 401

    validation_err = validate_json_with_desc(request.json, {
        'board': {
            'id': ('int', 'null'),
            'name': 'str',
            'public': 'bool',
            'columns': [{
                'id': ('int', 'null'),
                'name': 'str',
                'mandatory': 'bool',
            }],
            'deleted_columns': [{
                'id': 'int',
                'name': 'str',
                'mandatory': False,
            }],
            'subs': [{
                'user_id': 'int',
                'user_name': 'str',
                'owner': 'bool',
                'brand_new': 'bool',
            }],
            'deleted_subs': [{
                'user_id': 'int',
                'user_name': 'str',
                'owner': 'bool',
                'brand_new': False,
            }],
        }
    })
    if validation_err is not None:
        error_txt = "Requête incomplète. (%s)" % validation_err
        return error(error_txt), 400

    board_json = request.json['board']

    for column_json in board_json['columns']:
        if column_json['id'] is None:
            continue
        for deleted_column_json in board_json['deleted_columns']:
            if column_json['id'] == deleted_column_json['id']:
                return error(
                    "Colonne N°{} modifiée et supprimée en même temps."
                    .format(column_json['id'])), 400

    for sub_json in board_json['subs']:
        if sub_json['brand_new']:
            continue
        for deleted_sub_json in board_json['deleted_subs']:
            if sub_json['user_id'] == deleted_sub_json['user_id']:
                return error(
                    ("Sub avec utilisateur {} modifiée et"
                        + " supprimée en même temps.")
                    .format(sub_json['user_id'])), 400

    # TODO: test check that deleted_* and * dont overlap
    # TODO: add column/sub count limit ?

    has_owners = False
    for sub_json in board_json['subs']:
        if sub_json['owner']:
            has_owners = True
            break
    if not has_owners:
        return error("Aucun membre du tableau n'est gérant."), 400

    # Get/Create the board

    if board_json['id'] is not None:
        board = model.Board.query.get(board_json['id'])
    else:
        board = model.Board()

    board.name = board_json['name']
    board.public = board_json['public']

    # Columns

    for i, column_json in enumerate(board_json['columns']):
        if column_json['id'] is not None:
            column = model.Column.query.get(column_json['id'])
        else:  # new column
            column = model.Column(board=board)

        column.name = column_json['name']
        column.mandatory = column_json['mandatory']
        column.order = i
        db.session.add(column)

    for column_json in board_json['deleted_columns']:
        column = model.Column.query.get(column_json['id'])
        if column.board_id != board.id:
            return error(
                "La colonne N°{} ne fait pas partie du tableau"
                .format(column_json['id']))
        db.session.delete(column)

    # Subscriptions

    for sub_json in board_json['subs']:
        sub_user = model.User.query.get(sub_json['user_id'])
        if sub_user is None:
            return error(
                "L'utilisateur {} n'existe pas"
                .format(sub_json['user_id']))

        if sub_json['brand_new']:
            sub = model.Subscription(board=board, user=sub_user)
        else:
            sub = (
                model.Subscription.query
                .filter_by(board=board, user=sub_user)
                .one())

        sub.owner = sub_json['owner']
        db.session.add(sub)

    for sub_json in board_json['deleted_subs']:
        sub_user = model.User.query.get(sub_json['user_id'])
        if sub_user is None:
            return error("L'utilisateur {} n'existe pas"
                         .format(sub_json['user_id']))

        sub = (
            model.Subscription.query
            .filter_by(board=board, user=sub_user)
            .one())
        db.session.delete(sub)

    # Done !

    db.session.add(board)
    db.session.commit()

    return flask.jsonify(status='ok', board_id=board.id)
