import enum
import unicodedata
from typing import Optional, List
import flask
import argon2
from . import db, ph


class User(db.Model):
    __tablename__ = 'users'
    id = db.Column(db.Integer, primary_key=True)
    login = db.Column(db.Unicode(80), nullable=False, unique=True)
    display_name = db.Column(db.Unicode(160), nullable=False)
    password_hash = db.Column(db.String(1024), nullable=False)

    subs = db.relationship(
        (lambda: Subscription),
        back_populates='user',
        cascade='save-update, merge, delete, delete-orphan',
    )
    assigned_tasks = db.relationship((lambda: Task),
                                     back_populates='assigned_user')

    @staticmethod
    def get_from_login(login: str) -> Optional['User']:
        return User.query \
            .filter_by(login=User.normalize_login(login)) \
            .one_or_none()

    @staticmethod
    def normalize_login(login: str) -> str:
        return unicodedata.normalize('NFKC', login).casefold()

    @staticmethod
    def hash_password(password: str) -> str:
        return ph.hash(password)

    @staticmethod
    def create(login: str, display_name: str, password: str) -> 'User':
        password_hash = User.hash_password(password)
        return User(
            login=User.normalize_login(login),
            display_name=display_name,
            password_hash=password_hash,
        )

    @staticmethod
    def get_from_login_password(login: str, password: str) -> Optional['User']:
        """
            Retourne l'utilisateur correspondant à login si le mot de passe est
            correct, et None sinon.
            Si le mot de passe doit être rehaché, alors il est rehaché.
        """
        user = User.get_from_login(login)

        if user is None:
            return None

        if not user.is_password_valid(password):
            return None

        if ph.check_needs_rehash(user.password_hash):
            flask.current_app.logger.debug(
                'Rehashing password for {}'.format(user.login))

            user.password_hash = User.hash_password(password)
            db.session.commit()

        return user

    def is_password_valid(self, password: str) -> bool:
        try:
            ph.verify(self.password_hash, password)
        except argon2.exceptions.VerificationError:
            return False
        return True

    def get_board_subscription(self, board_id: int) \
            -> Optional['Subscription']:
        return Subscription.query \
            .filter_by(user_id=self.id, board_id=board_id) \
            .one_or_none()


class Board(db.Model):
    __tablename__ = 'boards'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.Unicode(160), nullable=False)
    public = db.Column(db.Boolean, nullable=False)

    columns = db.relationship(
        (lambda: Column),
        back_populates='board',
        cascade='save-update, merge, delete, delete-orphan',
        order_by='Column.order',
    )
    subs = db.relationship(
        (lambda: Subscription),
        back_populates='board',
        cascade='save-update, merge, delete, delete-orphan',
    )

    @staticmethod
    def get_public_boards() -> List['Board']:
        return Board.query.filter_by(public=True).all()

    def get_urlified_name(self) -> str:
        return (self.name
                # We just remove slashes, because some stuff like /settings are
                # inside the board "folder".
                .replace('/', '_')
                # Everything else (space, #, "...) is already handled by
                # url_for.
                )

    def get_url(self) -> str:
        return flask.url_for(
            'board_view.board_view',
            board_id=self.id,
            board_name=self.get_urlified_name(),
        )

    def get_settings_url(self) -> str:
        return flask.url_for(
            'board_settings.edition_page',
            board_id=self.id,
            board_name=self.get_urlified_name(),
        )

    def can_be_viewed_by(self, user: Optional[User]) -> bool:
        if self.public:
            return True
        return self.can_be_edited_by(user)

    def can_be_edited_by(self, user: Optional[User]) -> bool:
        if user is None:
            return False

        sub = user.get_board_subscription(self.id)

        return sub is not None

    def is_owned_by(self, user: Optional[User]) -> bool:
        if user is None:
            return False

        sub = user.get_board_subscription(self.id)

        return sub is not None and sub.owner

    def can_assign_user(self, assigner: Optional[User],
                        assigned_to: Optional[User],
                        old_assigned_to: Optional[User]) -> bool:
        """
            Un utilisateur peut en affecter un autre dans une tâche si:
                - L'utilisateur s'auto affecte la tâche.
                - L'utilisateur se désaffecte une tâche qui lui est attribuée
                - L'affecteur est le gérant du tableau.
        """
        if assigner is None or not self.can_be_edited_by(assigner):
            return False

        if assigned_to is None:
            if old_assigned_to is None \
                or (old_assigned_to is not None
                    and assigner.id == old_assigned_to.id):
                return True
        else:
            if assigner.id == assigned_to.id:
                return True

        if self.is_owned_by(assigner) and (
                assigned_to is None or self.can_be_edited_by(assigned_to)):
            return True

        return False

    def is_vetoing_user_deletion(self, user: Optional[User]) -> bool:
        if not self.is_owned_by(user):
            return False

        if len(self.subs) == 1:
            return False

        for sub in self.subs:
            if sub.user_id != user.id and sub.owner:
                return False

        return True


class Subscription(db.Model):
    __tablename__ = 'subscriptions'
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'),
                        primary_key=True)
    board_id = db.Column(db.Integer, db.ForeignKey('boards.id'),
                         primary_key=True)
    owner = db.Column(db.Boolean, nullable=False)

    user = db.relationship(lambda: User, back_populates='subs')
    board = db.relationship(lambda: Board, back_populates='subs')


class Column(db.Model):
    __tablename__ = 'columns'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.Unicode(160), nullable=False)
    mandatory = db.Column(db.Boolean, nullable=False)
    board_id = db.Column(db.Integer, db.ForeignKey('boards.id'),
                         nullable=False)
    order = db.Column(db.Integer, nullable=False)

    board = db.relationship(lambda: Board, back_populates='columns')
    tasks = db.relationship(
        lambda: Task,
        back_populates='column',
        cascade='save-update, merge, delete, delete-orphan',
    )


class TaskColor(enum.Enum):
    white = 1
    red = 2
    orange = 3
    yellow = 4
    green = 5
    blue = 6
    purple = 7
    pink = 8
    brown = 9
    grey = 10


class Task(db.Model):
    __tablename__ = 'tasks'
    id = db.Column(db.Integer, primary_key=True)
    column_id = db.Column(db.Integer, db.ForeignKey('columns.id'),
                          nullable=False)
    description = db.Column(db.Unicode(4096), nullable=False)
    color = db.Column(db.Enum(TaskColor), nullable=False)
    due_date = db.Column(db.Date, nullable=True)
    assigned_user_id = db.Column(db.Integer, db.ForeignKey('users.id'),
                                 nullable=True)

    column = db.relationship(lambda: Column, back_populates='tasks')
    assigned_user = db.relationship(lambda: User,
                                    back_populates='assigned_tasks')

    def can_be_deleted_by(self, user: Optional[User]) -> bool:
        """
            Un utilisateur peut déplacer / supprimer une tâche si:
            - Il est le propriétaire du tableau
            - La tâche n'est affectée à personne
            - La tâche est affectée à lui
        """
        if user is None:
            return False

        sub = user.get_board_subscription(self.column.board_id)

        if sub is None:
            return False

        if sub.owner:
            return True
        elif self.assigned_user_id is None:
            return True
        elif self.assigned_user_id == user.id:
            return True

        return False

    def can_be_moved_to(self, mover: Optional[User], column: Column) -> bool:
        if not column.board.can_be_edited_by(mover):
            return False

        # It's a new task
        if self.column is None:
            return True

        # No-op
        if self.column.id == column.id:
            return True

        # Can't move between boards
        if self.column.board_id != column.board_id:
            return False

        return self.can_be_deleted_by(mover)
