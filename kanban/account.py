import flask

from .auth import get_current_user
from . import model, db, apply_csp_with_nonce

bp = flask.Blueprint('account', __name__)


@bp.route('/my_account')
@apply_csp_with_nonce
def settings(csp_nonce: str):
    current_user = get_current_user()
    if current_user is None:
        return flask.render_template(
            'error_page.html',
            current_user=current_user,
            err_msg="Vous devez vous connecter pour accéder à cette page.",
        ), 401

    return flask.render_template(
        'account_settings.html',
        current_user=current_user,
        csp_nonce=csp_nonce,
    )


@bp.route('/post/change_profile', methods=['POST'])
def change_profile():
    current_user = get_current_user()
    if current_user is None:
        return flask.render_template(
            'error_page.html',
            current_user=current_user,
            err_msg="Vous devez vous connecter pour accéder à cette page.",
        ), 401

    if flask.request.form.get('display_name', '') == '':
        flask.flash("Nom d'affichage obligatoire", 'error')
        return flask.redirect(flask.url_for('account.settings'))

    current_user.display_name = flask.request.form['display_name']
    db.session.commit()

    flask.flash("Votre profil a bien été modifié.")
    return flask.redirect(flask.url_for('account.settings'))


@bp.route('/post/change_password', methods=['POST'])
def change_password():
    current_user = get_current_user()
    if current_user is None:
        return flask.render_template(
            'error_page.html',
            current_user=current_user,
            err_msg="Vous devez vous connecter pour accéder à cette page.",
        ), 401

    if flask.request.form.get('old_password', '') == '':
        flask.flash("Ancien mot de passe obligatoire.", 'error')
        return flask.redirect(flask.url_for('account.settings'))
    elif flask.request.form.get('new_password', '') == '':
        flask.flash("Nouveau mot de passe obligatoire.", 'error')
        return flask.redirect(flask.url_for('account.settings'))
    elif flask.request.form.get('new_password_redundancy', '') == '':
        flask.flash("Nouveau mot de passe obligatoire.", 'error')
        return flask.redirect(flask.url_for('account.settings'))

    old_password = flask.request.form['old_password']
    new_password = flask.request.form['new_password']
    new_password_redundancy = flask.request.form['new_password_redundancy']

    if new_password != new_password_redundancy:
        flask.flash("Les deux mots de passe ne sont pas identiques.", 'error')
        return flask.redirect(flask.url_for('account.settings'))

    if not current_user.is_password_valid(old_password):
        flask.flash(
            "L'ancien mot de passe n'est pas correct, veuillez réessayer.",
            'error')
        return flask.redirect(flask.url_for('account.settings'))

    current_user.password_hash = model.User.hash_password(new_password)
    db.session.commit()

    flask.flash("Le mot de passe a bien été changé.")
    return flask.redirect(flask.url_for('account.settings'))


@bp.route('/post/delete_account', methods=['POST'])
def delete_account():
    current_user = get_current_user()
    if current_user is None:
        return flask.render_template(
            'error_page.html',
            current_user=current_user,
            err_msg="Vous devez vous connecter pour accéder à cette page.",
        ), 401

    boards_vetoing = []

    for sub in current_user.subs:
        if sub.board.is_vetoing_user_deletion(current_user):
            boards_vetoing.append(sub.board)

    if len(boards_vetoing) > 0:
        boards_vetoing_str = ', '.join([b.name for b in boards_vetoing])
        flask.flash((
            "Vous ne pouvez pas supprimer votre compte, car les tableaux {} "
            + "n'ont pas de gérant pour vous remplacer. "
            + "Supprimez ces tableaux manuellement, ou affectez l'un des "
            + "membres comme gérant du tableau, puis réessayez."
            ).format(boards_vetoing_str))
        return flask.redirect(flask.url_for('account.settings'))

    # Delete boards with only this user as member

    for sub in current_user.subs:
        if len(sub.board.subs) == 1:
            db.session.delete(sub.board)

    db.session.delete(current_user)
    db.session.commit()

    flask.flash("Votre compte a bien été supprimé.")
    return flask.redirect(flask.url_for('homepage.homepage'))
