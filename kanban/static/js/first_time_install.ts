/// <reference path="common.ts"/>
"use strict";

interface DBEngine {
    name: string;
    type: 'path' | 'uri_server' | 'custom';
    prefix: string;
    // apis: string[];
}

const db_engines: DBEngine[] = [
    {
        name: 'SQLite',
        type: 'path',
        prefix: 'sqlite',
    }, {
        name: 'Postgres SQL',
        type: 'uri_server',
        prefix: 'postgresql',
    }, {
        name: 'MySQL',
        type: 'uri_server',
        prefix: 'mysql+pymysql',
    }, {
        name: 'Oracle',
        type: 'uri_server',
        prefix: 'oracle',
    }, {
        name: 'Microsoft SQL',
        type: 'uri_server',
        prefix: 'mssql',
    }, {
        name: 'Autre',
        type: 'custom',
        prefix: '',
    },
];

var current_db_engine: DBEngine = db_engines[0];

function select_db_engine(engine: DBEngine) {
    current_db_engine = engine;

    $.id('db-engine-uri-server').hidden = engine.type !== 'uri_server';
    $.id('db-engine-path').hidden = engine.type !== 'path';
    $.id('db-engine-custom').hidden = engine.type !== 'custom';

    for (const link of $.class_('db-engine-link')) {
        link.classList.remove('selected');
    }
    $.id('db-' + engine.prefix).classList.add('selected');

    update_final_db_uri();
}

function get_final_db_uri(): string {
    if (current_db_engine.type === 'custom') {
        return (<HTMLInputElement>$.id('custom-uri')).value;
    }
    let uri = current_db_engine.prefix;

    uri += '://';

    if (current_db_engine.type === 'uri_server') {
        const username = (<HTMLInputElement>$.id('uri-server-username')).value;
        const password = (<HTMLInputElement>$.id('uri-server-password')).value;
        const host = (<HTMLInputElement>$.id('uri-server-host')).value;
        const database = (<HTMLInputElement>$.id('uri-server-database')).value;
        if (username !== '') {
            uri += encodeURIComponent(username);
            if (password !== '') {
                uri += ':' + encodeURIComponent(password);
            }
            uri += '@';
        }
        uri += host;
        if (database !== '') {
            uri += '/' + database;
        }
    } else if (current_db_engine.type === 'path') {
        uri += '/' + (<HTMLInputElement>$.id('path-path')).value;
    }

    return uri;
}

function update_final_db_uri() {
    let input = <HTMLInputElement>$.id('final-db-uri');
    input.value = get_final_db_uri();
}

{
    let ul = $.id('db-engines');

    for (const engine of db_engines) {
        let li = $.newElement('li');
        ul.appendChild(li);

        let a = $.newElement('a', "db-" + engine.prefix, 'db-engine-link', engine.name);
        li.appendChild(a);

        a.setAttribute('href', '#');
        a.addEventListener('click', (ev) => {
            select_db_engine(engine);
        });
        $.id('uri-server-username').addEventListener('input', (_) => update_final_db_uri());
        $.id('uri-server-password').addEventListener('input', (_) => update_final_db_uri());
        $.id('uri-server-host').addEventListener('input', (_) => update_final_db_uri());
        $.id('uri-server-database').addEventListener('input', (_) => update_final_db_uri());
        $.id('path-path').addEventListener('input', (_) => update_final_db_uri());
        $.id('custom-uri').addEventListener('input', (_) => update_final_db_uri());
    }

    select_db_engine(current_db_engine);
}
