"use strict";
var $ = {
    id(id) {
        const elem = document.getElementById(id);
        if (elem == null) {
            throw Error("Element of id " + id + " not found.");
        }
        return elem;
    },
    class_(class_name) {
        return document.getElementsByClassName(class_name);
    },
    tagName(tag_name) {
        return document.getElementsByTagName(tag_name);
    },
    newElement(tag_name, id = null, classes = null, childs = null) {
        let elem = document.createElement(tag_name);
        if (id != null) {
            elem.id = id;
        }
        if (typeof (classes) === 'string') {
            elem.classList.add(classes);
        }
        else if (Array.isArray(classes)) {
            for (const class_ of classes) {
                elem.classList.add(class_);
            }
        }
        if (typeof childs === 'string') {
            elem.appendChild($.newTextNode(childs));
        }
        else if (Array.isArray(childs)) {
            for (const child of childs) {
                elem.appendChild(child);
            }
        }
        else if (childs != null) {
            elem.appendChild(childs);
        }
        return elem;
    },
    newTextNode(content) {
        return document.createTextNode(content);
    },
    empty(element) {
        while (element.firstChild != null) {
            element.removeChild(element.firstChild);
        }
    }
};
function ajax(url, method = 'GET', params = null, mime = null) {
    return new Promise(function (resolve, reject) {
        function urlEncode(params) {
            return Object.keys(params).map((key) => encodeURIComponent(key) +
                '=' + encodeURIComponent(params[key])).join('&');
        }
        const req = new XMLHttpRequest();
        if (method === 'GET' && params != null) {
            if (typeof (params) == 'string') {
                throw "params should not be a string when method == GET";
            }
            url += '?' + urlEncode(params);
        }
        req.open(method, url);
        if (method === 'GET') {
            params = null;
        }
        else {
            if (params != null && typeof (params) === 'object') {
                params = urlEncode(params);
            }
            if (mime != null) {
                req.setRequestHeader('Content-Type', mime);
            }
            else if (params != null) {
                req.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
            }
        }
        req.onload = () => { resolve(req); };
        req.onerror = () => { reject(req); };
        req.send(params);
    });
}
function ajaxSendJson(url, object, method = 'POST') {
    return ajax(url, method, JSON.stringify(object), 'application/json');
}
