"use strict";

// not done yet, bbcode is working just fine anyway.

function parseInlineMarkdown(src) {
    let i = 0;
    let nodes = [];
    let delimiter_stack = [];

    while (i < src.length) {
        let [token, new_index] = getToken(src, i);
        i = new_index;
        if (token == null) {
            break;
        }
        console.log(token);

        switch (token.type) {
            case "start": {
                let node = [token.content];
                nodes.push(node);
            } break;
            case "_":
            case "*": {
                let node = [token.content];
                nodes.push(node);
                delimiter_stack.push({
                    token: token,
                    node: node,
                    can_open: true,
                    can_close: true,
                });
            } break;
        }
    }

    function process_emphasis(stack_bottom = null) {
        let cur_pos = stack_bottom == null ? 0 : (stack_bottom + 1);
        let openers_bottom = {
            '*': stack_bottom,
            '_': stack_bottom,
        };

        while (true) {
            while (cur_pos < delimiter_stack.length && delimiter_stack[cur_pos].can_close) {
                cur_pos += 1;
            }

            const close_tag = delimiter_stack[cur_pos];

            for (let i = cur_pos - 1; i >= stack_bottom && i >= openers_bottom[close_tag.token.type]; i--) {
                if (delimiter_stack[i].can_open && delimiter_stack[i].token.type === close_tag.token.type) {
                    const open_tag = delimiter_stack[i];
                    let is_strong = false;
                    if (open_tag.token.count >= 2 && close_tag.token.count >= 2) {
                        is_strong = true;
                    }

                    const open_index = nodes.indexOf(open_tag.node);
                    nodes.splice(open_index + 1, 0, [is_strong ? '[b]' : '[i]']);

                    delimiter_stack.splice(cur_pos, );
                }
            }
            // if none found
        }

    }

    function getToken(src, start_index) /* -> [token, new_index] */ {
        let i = start_index;
        let state = "start";
        let content = "";
        let state_context = {};

        while (i < src.length) {
            const c = src[i];
            if (state === "start") {
                if (c === '*' || c === '_') {
                    if (content !== "") {
                        return [{type: 'text', content: content}, i];
                    }
                    state = "items";
                    content = c;
                    state_context = { char: c, count: 1 };
                } else {
                    content += c;
                }
            } else if (state === "items") {
                if (c === state_context.char) {
                    content += c;
                    state_context.count += 1;
                } else {
                    return [{type: state_context.char, content: content, count: state_context.count}, i];
                }
            }
            i++;
        }

        if (state === "start") {
            if (content !== "")
                return [{type: 'text', content: content}, i];
        } else if (state === "items") {
            return [{type: state_context.char, content: content, count: state_context.count}, i];
        }
        return [null, i];
    }
}
