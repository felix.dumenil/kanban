"use strict";

/*
    Parse a subset of BBCode, and return a <span> Element.
    If a tag is malformed, it's printed as-is.
    '\[' is printed as '['.
    The following tags are supported :
        - [b]
        - [i]
        - [u]
        - [s] and [strike]
    If a tag is not in the list above, it produces a <span> element instead.
*/
function parseBBCode(src: string): HTMLElement {

    interface NodeText {
        type: 'text';
        content: string;
    }
    interface NodeTag {
        type: 'tag';
        tag_name: string;
        childs: Node[];
        closed: boolean;
    }
    interface NodeLineBreak {
        type: 'linebreak';
    }
    interface NodeRoot {
        type: 'root';
        childs: Node[];
    }

    type NodeWithChilds = NodeTag | NodeRoot;

    type Node = NodeText | NodeLineBreak | NodeWithChilds;

    let tree: NodeRoot = {type: 'root', childs: []};
    let stack: NodeWithChilds[] = [tree];

    while (true) {
        const [token, next_i] = readToken(src);
        if (token == null) break;
        src = src.slice(next_i);

        if (token.type === 'text') {
            stack[stack.length-1].childs.push({
                type: 'text',
                content: token.content,
            });
        } else if (token.type === 'tag') {
            if (! token.is_close_tag) {
                const tag_node: NodeTag = {
                    type: 'tag',
                    tag_name: token.tag_name,
                    childs: [],
                    closed: false,
                };
                stack[stack.length-1].childs.push(tag_node);
                stack.push(tag_node);
            } else {
                const node = stack[stack.length-1];
                if (node.type === 'tag'
                        && node.tag_name === token.tag_name) {
                    // close the tag
                    node.closed = true;
                    stack.pop();
                } else {
                    // we found a misplaced close tag
                    // ex : aaa[b]bbb[/i]ccc
                    // in this case, we just print the close tag as text
                    stack[stack.length-1].childs.push({
                        type: 'text',
                        content: token.raw,
                    });
                }
            }
        } else if (token.type === 'linebreak') {
            stack[stack.length-1].childs.push({ type: 'linebreak' });
        }
    }

    return <HTMLElement>nodeToHTMLNode(tree);

    interface TokenTag {
        type: 'tag';
        is_close_tag: boolean;
        tag_name: string;
        raw: string;
    }
    interface TokenText {
        type: 'text';
        content: string;
    }
    interface TokenLineBreak {
        type: 'linebreak';
    }


    type Token = TokenTag | TokenText | TokenLineBreak;

    // Read the first token in src. Return [token, token_length].
    function readToken(src: string): [Token | null, number] {
        let i = 0;

        if (src[i] === '[') { // parsing a tag
            function reject(): [TokenText, number] {
                return [{type: 'text', content: src.slice(0, i)}, i];
            }

            i++;
            if (i >= src.length) return reject();

            let is_close_tag = false;
            if (src[i] === '/') {
                is_close_tag = true;
                i++;
                if (i >= src.length) return reject();
            }

            // tag name

            let tag_name = '';
            while (i < src.length && src[i] !== ']') {
                const c = src[i].toLowerCase();
                i++;
                if ('a' > c || c > 'z') return reject();
                tag_name += c;
            }

            if (tag_name === '' || i >= src.length) return reject();

            const char_end = src[i];
            i++;

            if (char_end === ']') {
                return [{
                    type: 'tag',
                    is_close_tag: is_close_tag,
                    tag_name: tag_name,
                    raw: src.slice(0, i)
                }, i];
            } else {
                return reject();
            }

        } else if (src[i] === '\n') { // parsing newline
            return [{type: 'linebreak'}, i+1];
        } else { // parsing text
            let buf = "";

            while (i < src.length) {
                const c = src[i];
                if (c === '[' || c === '\n') {
                    return [{type: 'text', content: buf}, i];
                } else if (c === '\\' && i+1 < src.length && src[i+1] === '[') {
                    i++;
                    buf += src[i];
                } else {
                    buf += src[i];
                }
                i++;
            }
            if (buf !== '') {
                return [{type: 'text', content: buf}, i];
            }
        }
        return [null, i];
    }

    function nodeToHTMLNode(node : Node): HTMLElement | Text {
        if (node.type === 'text') {
            return document.createTextNode(node.content);
        } else if (node.type === 'linebreak') {
            return document.createElement('br');
        }

        let add_close_tag_as_text = false;
        let element: HTMLElement;
        if (node.type === 'root') {
            element = document.createElement('span');
        } else if (node.type === 'tag') {
            switch (node.tag_name) {
                case 'b': {
                    element = document.createElement('strong');
                } break;
                case 'i': {
                    element = document.createElement('em');
                } break;
                case 'u': {
                    element = document.createElement('u');
                } break;
                case 'strike': case 's': {
                    element = document.createElement('del');
                } break;
                default: {
                    element = document.createElement('span');
                    element.appendChild(document.createTextNode('['+ node.tag_name +']'));
                    if (node.closed) {
                        add_close_tag_as_text = true;
                    }
                } break;
            }
        } else throw "unreachable";

        for (const child of node.childs) {
            element.appendChild(nodeToHTMLNode(child));
        }

        if (add_close_tag_as_text) {
            element.appendChild(document.createTextNode(
                '[/'+ (<NodeTag>node).tag_name +']'));
        }

        return element;
    }
}

function testBBCode(): void {
    let tests_done = 0;
    let tests_failed = 0;

    function test(src: string, expected: string): void {
        tests_done++;
        const src_html = parseBBCode(src).innerHTML;

        if (src_html !== expected) {
            tests_failed++;
            console.error(`Expected '${expected}' for '${src}', got '${src_html}'`);
        }
    }

    test('abcd', 'abcd');
    test('abcd[b]bold[/b]def', 'abcd<strong>bold</strong>def');
    test('abcd<b>bold</b>def', 'abcd&lt;b&gt;bold&lt;/b&gt;def');
    test('abcd[i]italics[/i]def', 'abcd<em>italics</em>def');
    test('abcd[u]underlined[/u]def', 'abcd<u>underlined</u>def');
    test('abcd[s]strike[/s]def', 'abcd<del>strike</del>def');
    test('abcd[strike]strike[/strike]def', 'abcd<del>strike</del>def');
    test('abcd[StrIKE]strike[/StrIKE]def', 'abcd<del>strike</del>def');
    test('abcd[b]aaa', 'abcd<strong>aaa</strong>');
    test('abcd[b]', 'abcd<strong></strong>');
    // bad tags
    test('abcd[baaa', 'abcd[baaa');
    test('abcd[b', 'abcd[b');
    test('abcd[', 'abcd[');
    test('abcd[/b]def', 'abcd[/b]def');
    test('abcd[]def', 'abcd[]def');
    test('abcd[strike]strike[/S]def', 'abcd<del>strike[/S]def</del>');
    test('abcd[def[/b]', 'abcd[def[/b]');
    test('abcd[whatever]def', 'abcd<span>[whatever]def</span>');
    test('abcd[whatever]def[/what', 'abcd<span>[whatever]def[/what</span>');
    test('abcd[whatever]def[/whatever]', 'abcd<span>[whatever]def[/whatever]</span>');
    // escaping
    test('abcd\\def', 'abcd\\def');
    test('abcd\\[def', 'abcd[def');

    console.log(`${tests_failed} failed tests / ${tests_done} total tests`);
}
