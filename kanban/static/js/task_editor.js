/// <reference path="common.ts"/>
/// <reference path="board_view.ts"/>
"use strict";
const colors_display_text = {
    white: 'Blanc',
    red: 'Rouge',
    orange: 'Orange',
    yellow: 'Jaune',
    green: 'Vert',
    blue: 'Bleu',
    purple: 'Violet',
    pink: 'Rose',
    brown: 'Marron',
    grey: 'Gris',
};
class TaskEditor {
    constructor() {
        this.task = null;
        this.submitting_task = false;
        this.overlay = $.id('task-editor-overlay');
        this.overlay.addEventListener('click', (ev) => {
            if (ev.target === this.overlay) {
                this.clickCancel();
            }
        });
        this.title = $.id('task-editor-title');
        this.desc_editor = new BBCodeEditor($.id('task-editor-description'), $.id('task-editor-formatting-bar'), (new_content) => this.descriptionChanged(new_content));
        this.assigned_user_select = $.id('task-editor-assigned-user');
        this.assigned_user_select.addEventListener('change', (ev) => this.assignedUserChanged(ev));
        this.due_date_input = $.id('task-editor-due-date');
        this.due_date_input.addEventListener('change', (ev) => this.dueDateChanged(ev));
        this.color_selector = new ColorSelector($.id('task-editor-colors'), $.id('task-editor-task'), (color) => this.setColor(color));
        this.save_button = $.id('task-editor-save-button');
        this.delete_button = $.id('task-editor-delete-button');
        this.cancel_button = $.id('task-editor-cancel-button');
        this.save_button.addEventListener('click', (ev) => this.clickSave());
        this.delete_button.addEventListener('click', (ev) => this.clickDelete());
        this.cancel_button.addEventListener('click', (ev) => this.clickCancel());
    }
    buildAssignToSelect() {
        if (this.task == null)
            throw "[unreachable] task is null";
        $.empty(this.assigned_user_select);
        const assigned_user_id = this.task.assigned_user_id;
        let permissions;
        // you're the owner => do whatever you want
        if (current_user_is_owner) {
            permissions = 'owner';
            // normal permissions => can switch the assigned user to null or yourself
        }
        else if (assigned_user_id == null
            || assigned_user_id === current_user_id) {
            permissions = 'normal';
            // task is assigned to someone else => back off
        }
        else {
            permissions = 'dont_touch';
        }
        {
            let option_none = $.newElement('option', null, null, 'Personne');
            option_none.value = 'none';
            if (assigned_user_id == null)
                option_none.selected = true;
            if (permissions === 'dont_touch')
                option_none.disabled = true;
            this.assigned_user_select.appendChild(option_none);
        }
        for (const sub of board_viewer.board.subs) {
            let label = sub.user_name;
            if (sub.user_id === current_user_id) {
                label += ' (Vous)';
            }
            let option = $.newElement('option', null, null, label);
            option.value = String(sub.user_id);
            if (assigned_user_id == sub.user_id)
                option.selected = true;
            if (sub.user_id !== assigned_user_id) {
                if (permissions === 'dont_touch') {
                    option.disabled = true;
                }
                else if (permissions === 'normal'
                    && sub.user_id !== current_user_id) {
                    option.disabled = true;
                }
            }
            this.assigned_user_select.appendChild(option);
        }
    }
    assignedUserChanged(ev) {
        if (this.task == null)
            throw "[unreachable] task is null";
        const option = ev.target;
        const val = option.value;
        if (val === 'none') {
            this.task.assigned_user_id = null;
        }
        else {
            const val_num = parseInt(val, 10);
            if (isNaN(val_num))
                throw "[unreachable] option value is not 'none' or an int";
            this.task.assigned_user_id = val_num;
        }
    }
    dueDateChanged(ev) {
        if (this.task == null)
            throw "[unreachable] task is null";
        if (this.due_date_input.value === '') {
            this.task.due_date = null;
        }
        else {
            this.task.due_date = new Date(this.due_date_input.value);
        }
    }
    isTaskNew() {
        return this.task.id == null;
    }
    openTask(task = null, column_id = null) {
        if (task == null) {
            // new task
            this.task = {
                id: null,
                column_id: column_id,
                description: '',
                color: board_view.TaskColor.white,
                assigned_user_id: null,
                due_date: null,
            };
            this.delete_button.hidden = true;
            $.empty(this.title);
            this.title.appendChild($.newTextNode("Création d'une tâche"));
        }
        else {
            // editing the task, let's make a copy
            this.task = {
                id: task.id,
                column_id: column_id,
                description: task.description,
                color: task.color,
                assigned_user_id: task.assigned_user_id,
                due_date: task.due_date,
            };
            this.delete_button.hidden = false;
            $.empty(this.title);
            this.title.appendChild($.newTextNode("Modification de la tâche"));
        }
        // color
        this.color_selector.updateColor(this.task.color);
        // due date
        if (this.task.due_date == null) {
            this.due_date_input.value = '';
        }
        else {
            this.due_date_input.valueAsDate = this.task.due_date;
        }
        // description
        this.desc_editor.setContent(this.task.description);
        this.desc_editor.focus();
        this.updatePreview();
        // assigned user
        this.buildAssignToSelect();
        // show it
        $.tagName('body').item(0).classList.add('blur');
        this.overlay.hidden = false;
    }
    close() {
        $.tagName('body').item(0).classList.remove('blur');
        this.overlay.hidden = true;
    }
    lockInterface(reason) {
        this.submitting_task = true;
        this.color_selector.disabled = true;
        this.save_button.setAttribute('disabled', 'disabled');
        this.delete_button.setAttribute('disabled', 'disabled');
        this.cancel_button.setAttribute('disabled', 'disabled');
        this.assigned_user_select.setAttribute('disabled', 'disabled');
        this.due_date_input.setAttribute('disabled', 'disabled');
        if (reason === 'save') {
            this.save_button.blur();
            this.save_button.classList.add('loading');
        }
        else if (reason === 'delete') {
            this.delete_button.blur();
            this.delete_button.classList.add('loading');
        }
    }
    unlockInterface(reason) {
        this.submitting_task = false;
        this.color_selector.disabled = false;
        this.save_button.removeAttribute('disabled');
        this.delete_button.removeAttribute('disabled');
        this.cancel_button.removeAttribute('disabled');
        this.assigned_user_select.removeAttribute('disabled');
        this.due_date_input.removeAttribute('disabled');
        if (reason === 'save') {
            this.save_button.classList.remove('loading');
        }
        else if (reason === 'delete') {
            this.delete_button.classList.remove('loading');
        }
    }
    descriptionChanged(new_content) {
        if (this.submitting_task)
            throw "[unreachable] description shouldn't be editable";
        if (this.task == null)
            throw "[unreachable] task is null";
        this.task.description = new_content;
        this.updatePreview();
    }
    updatePreview() {
        if (this.task == null)
            throw "[unreachable] task is null";
        const desc_bbcode = this.task.description;
        const desc_tree = parseBBCode(desc_bbcode);
        let preview_div = $.id('task-editor-description-preview');
        $.empty(preview_div);
        preview_div.appendChild(desc_tree);
    }
    setColor(color) {
        if (this.submitting_task)
            throw "[unreachable] color shouldn't be editable";
        if (this.task == null)
            throw "[unreachable] task is null";
        this.task.color = color;
    }
    prepareTaskForSubmission(task) {
        return {
            id: task.id,
            column_id: task.column_id,
            description: task.description,
            color: task.color,
            assigned_user_id: task.assigned_user_id,
            due_date: (() => {
                if (task.due_date == null)
                    return null;
                // we want a date with the format "YYYY-MM-DD"
                const whole_date = task.due_date.toISOString();
                // .toISOString() gives something like "YYYY-MM-DDTwhatever"
                const only_the_date = whole_date.slice(0, 4 + 1 + 2 + 1 + 2);
                return only_the_date;
            })(),
        };
    }
    async saveTask() {
        if (this.task == null)
            throw "[unreachable] task is null";
        let req;
        try {
            req = await ajaxSendJson(script_root + '/post/edit_task', { task: this.prepareTaskForSubmission(this.task) });
        }
        catch (err_req) {
            throw err_req.statusText || "Erreur de contact avec le serveur";
        }
        let response;
        try {
            response = JSON.parse(req.responseText);
        }
        catch (json_err) {
            console.error(json_err);
            throw "Erreur de contact avec le serveur";
        }
        if (response.status !== 'ok') {
            throw response.msg;
        }
        this.task.id = response.task_id;
    }
    async clickSave() {
        if (this.submitting_task)
            return;
        if (this.task == null)
            throw "[unreachable] task is null";
        this.lockInterface('save');
        try {
            await this.saveTask();
        }
        catch (e) {
            console.error(e);
            alert("Impossible d'enregistrer la tâche : " + e);
            return;
        }
        finally {
            this.unlockInterface('save');
        }
        // we know the task has an id, so the cast is safe
        board_viewer.editedTask(this.task);
        this.close();
    }
    async deleteTask() {
        if (this.task == null)
            throw "[unreachable] task is null";
        if (this.isTaskNew())
            throw "[unreachable] new tasks can't be deleted";
        let req;
        try {
            req = await ajaxSendJson(script_root + '/post/delete_task', { task_id: this.task.id });
        }
        catch (err_req) {
            throw err_req.statusText || "Erreur de contact avec le serveur";
        }
        let response;
        try {
            response = JSON.parse(req.responseText);
        }
        catch (json_err) {
            console.error(json_err);
            throw "Erreur de contact avec le serveur";
        }
        if (response.status !== 'ok') {
            throw response.msg;
        }
    }
    async clickDelete() {
        if (this.submitting_task)
            return;
        if (this.task == null)
            throw "[unreachable] task is null";
        if (this.isTaskNew())
            throw "[unreachable] new tasks can't be deleted";
        if (!board_view.canMoveOrDeleteTask(this.task)) {
            alert("Vous ne pouvez pas supprimer cette tâche, car elle est "
                + "affectée à un autre utilisateur.");
            return;
        }
        if (!confirm('Voulez-vous vraiment supprimer cette tâche ?'))
            return;
        this.lockInterface('delete');
        try {
            await this.deleteTask();
        }
        catch (e) {
            console.error(e);
            alert("Impossible de supprimer la tâche : " + e);
            return;
        }
        finally {
            this.unlockInterface('delete');
        }
        board_viewer.deletedTask(this.task.column_id, this.task.id);
        this.close();
    }
    clickCancel() {
        if (!this.submitting_task) {
            this.close();
        }
    }
}
class ColorSelector {
    constructor(container, task_elem, cb) {
        this.disabled = false;
        this.color_selected = null;
        this.color_selected_cb = cb;
        this.container = container;
        this.task_elem = task_elem;
        let color_buttons = {};
        $.empty(this.container);
        for (const color of board_view.color_order) {
            let button = $.newElement('div', null, ['task-color', 'task-' + color]);
            button.addEventListener('click', (ev) => this.clickColor(color));
            button.setAttribute('title', colors_display_text[color]);
            this.container.appendChild(button);
            color_buttons[color] = button;
        }
        this.color_buttons = color_buttons;
    }
    clickColor(color) {
        if (this.disabled)
            return;
        this.updateColor(color);
        this.color_selected_cb(color);
    }
    // update the visual
    updateColor(color) {
        if (this.color_selected != null) {
            this.task_elem.classList.remove('task-' + this.color_selected);
            this.color_buttons[this.color_selected].classList.remove('selected');
        }
        this.color_selected = color;
        this.task_elem.classList.add('task-' + color);
        this.color_buttons[color].classList.add('selected');
    }
}
class BBCodeEditor {
    constructor(textarea, formatting_bar, on_change) {
        this.textarea = textarea;
        this.formatting_bar = formatting_bar;
        this.on_change = on_change;
        this.textarea.addEventListener('input', (ev) => this.on_change(this.textarea.value));
        const format_tags = [
            { tag: 'b', display: 'Gras' },
            { tag: 'i', display: 'Italique' },
            { tag: 'u', display: 'Souligné' },
            { tag: 's', display: 'Barré' },
        ];
        for (const format_tag of format_tags) {
            let li = $.newElement('li');
            let a = $.newElement('a');
            a.setAttribute('href', '#');
            // do something like '[b]\[b]Gras\[/b][/b]'
            const t = format_tag.tag;
            const bbcode = [
                '[', t, ']\\[', t, ']',
                format_tag.display,
                '\\[/', t, '][/', t, ']',
            ].join('');
            a.appendChild(parseBBCode(bbcode));
            a.addEventListener('click', (ev) => this.clickFormat(format_tag.tag));
            li.appendChild(a);
            formatting_bar.appendChild(li);
        }
    }
    setContent(content) {
        this.textarea.value = content;
    }
    focus() {
        this.textarea.focus();
    }
    clickFormat(tag) {
        // If the user didn't select anything, then selectionStart == selectionEnd.
        const start = this.textarea.selectionStart;
        const end = this.textarea.selectionEnd;
        const content = this.textarea.value;
        const tag_start = `[${tag}]`;
        const tag_end = `[/${tag}]`;
        const before = content.slice(0, start);
        const between = content.slice(start, end);
        const after = content.slice(end);
        const new_content = before + tag_start + between + tag_end + after;
        this.textarea.value = new_content;
        this.textarea.focus();
        this.textarea.setSelectionRange(start + tag_start.length, end + tag_start.length);
        this.on_change(this.textarea.value);
    }
}
