/// <reference path="common.ts"/>
"use strict";
const drag_column_mime = 'application/x-kanban.drag_column';
class BoardEditor {
    constructor(board) {
        if (board == null) {
            board = {
                id: null,
                name: '',
                public: false,
                columns: [
                    { id: null, name: 'Stories', mandatory: true },
                    { id: null, name: 'En cours', mandatory: false },
                    { id: null, name: 'Terminées', mandatory: true },
                ],
                deleted_columns: [],
                subs: [{
                        user_id: current_user_id,
                        user_name: current_user_name,
                        owner: true,
                        brand_new: true,
                    }],
                deleted_subs: [],
            };
        }
        this.board = board;
        this.column_editor = new ColumnEditor(board);
        this.subscription_editor = new SubscriptionEditor(board);
        this.board_name_input = $.id('board_name');
        this.board_public_checkbox = $.id('board_public');
        this.board_name_input.addEventListener('input', (ev) => {
            this.board.name = this.board_name_input.value;
        });
        this.board_public_checkbox.addEventListener('change', (ev) => {
            this.board.public = this.board_public_checkbox.checked;
        });
        $.id('form_board_creation').addEventListener('submit', (ev) => {
            ev.preventDefault();
            this.submit();
        });
    }
    rebuild() {
        this.board_name_input.value = this.board.name;
        this.board_public_checkbox.checked = this.board.public;
        this.column_editor.rebuildColumns();
        this.subscription_editor.rebuildSubs();
    }
    async submit() {
        const data = {
            'board': this.board,
        };
        let req = await ajaxSendJson(script_root + '/post/board_settings', data);
        let response;
        try {
            response = JSON.parse(req.responseText);
        }
        catch (err) {
            alert("Le serveur n'a pas répondu correctement, veuillez reéssayer.");
            return;
        }
        if (response.status === 'ok') {
            window.location.href = script_root + '/board/' + String(response.board_id);
        }
        else {
            alert('Erreur : ' + response.msg);
        }
    }
}
class ColumnEditor {
    constructor(board) {
        this.board = board;
        this.col_list_element = $.id('board_column_list');
        $.id('board_add_column').addEventListener('click', (ev) => {
            this.addColumn();
        });
    }
    rebuildColumns() {
        $.empty(this.col_list_element);
        for (let i = 0; i < this.board.columns.length; i++) {
            const column = this.board.columns[i];
            this.col_list_element.appendChild(this.buildColumnElement(column, i));
        }
    }
    buildColumnElement(column, i) {
        let li = document.createElement('li');
        li.classList.add('board_column_list_item');
        if (column.mandatory) {
            li.classList.add('mandatory');
            let column_name = document.createElement('div');
            column_name.classList.add('column_name');
            column_name.appendChild(document.createTextNode(column.name));
            li.appendChild(column_name);
            let lock_icon = document.createElement('div');
            lock_icon.classList.add('lock_icon');
            lock_icon.appendChild(document.createTextNode('🔒'));
            li.appendChild(lock_icon);
        }
        else {
            li.setAttribute('draggable', 'true');
            let drag_handle = document.createElement('div');
            drag_handle.classList.add('drag_handle');
            drag_handle.appendChild(document.createTextNode('☰'));
            li.appendChild(drag_handle);
            li.addEventListener('dragstart', (ev) => {
                ev.dataTransfer.dropEffect = "move";
                ev.dataTransfer.setData(drag_column_mime, String(i));
                li.classList.add('dragged');
                this.col_list_element.classList.add('dragging');
            });
            li.addEventListener('dragend', (ev) => {
                li.classList.remove('dragged');
                this.col_list_element.classList.remove('dragging');
            });
            li.addEventListener('dragover', (ev) => {
                ev.preventDefault();
                ev.dataTransfer.dropEffect = "move";
            });
            li.addEventListener('dragenter', (ev) => {
                li.classList.add('dragover');
            });
            li.addEventListener('dragleave', (ev) => {
                li.classList.remove('dragover');
            });
            li.addEventListener('drop', (ev) => {
                ev.preventDefault();
                const index_from = Number(ev.dataTransfer.getData(drag_column_mime));
                this.swap(index_from, i);
            });
            let column_name = document.createElement('input');
            column_name.type = 'text';
            column_name.classList.add('column_name');
            column_name.value = column.name;
            column_name.addEventListener('change', (ev) => {
                column.name = column_name.value;
            });
            column_name.addEventListener('input', (ev) => {
                column.name = column_name.value;
            });
            // needed because you can't use your mouse in the field if the li is draggable
            column_name.addEventListener('mouseenter', (ev) => {
                li.setAttribute('draggable', 'false');
            });
            column_name.addEventListener('mouseleave', (ev) => {
                li.setAttribute('draggable', 'true');
            });
            li.appendChild(column_name);
            let delete_icon = document.createElement('div');
            delete_icon.classList.add('delete_icon');
            delete_icon.appendChild(document.createTextNode('✕'));
            delete_icon.addEventListener('click', (ev) => {
                this.removeColumn(i);
            });
            li.appendChild(delete_icon);
        }
        return li;
    }
    addColumn() {
        let insert_index = this.board.columns.length;
        while (insert_index > 0 && this.board.columns[insert_index - 1].mandatory) {
            insert_index--;
        }
        let new_column = {
            id: null,
            name: 'Nouvelle colonne',
            mandatory: false,
        };
        this.board.columns.splice(insert_index, 0, new_column);
        this.rebuildColumns();
    }
    removeColumn(i) {
        const column = this.board.columns[i];
        if (column.mandatory)
            throw "trying to delete a mandatory column";
        this.board.columns.splice(i, 1);
        if (column.id != null) {
            this.board.deleted_columns.push(column);
        }
        this.rebuildColumns();
    }
    swap(i, j) {
        if (this.board.columns[i].mandatory)
            throw "trying to swap a mandatory column";
        if (this.board.columns[j].mandatory)
            throw "trying to swap a mandatory column";
        const tmp = this.board.columns[i];
        this.board.columns[i] = this.board.columns[j];
        this.board.columns[j] = tmp;
        this.rebuildColumns();
    }
}
class SubscriptionEditor {
    constructor(board) {
        this.board = board;
        this.search_results = null;
        this.search_error = null;
        this.list_element = $.id('board_sub_list');
        this.search_title = $.id('board_sub_search_title');
        this.search_result_list = $.id('board_sub_search_result');
        this.search_bar = $.id('board_sub_search_bar');
        this.search_bar.addEventListener('input', (ev) => {
            this.startSearchPotentialMembers();
        });
        this.search_query_manager = new QueryManager((query) => this.executeSearchPotentialMembers(query), (query, req) => this.treatSearchPotentialMembers(query, req));
    }
    rebuildSubs() {
        $.empty(this.list_element);
        for (let i = 0; i < this.board.subs.length; i++) {
            const sub = this.board.subs[i];
            this.list_element.appendChild(this.buildSubscriptionElement(sub, i));
        }
    }
    buildSubscriptionElement(sub, i) {
        let li = $.newElement('li');
        li.classList.add('board_sub_list_item');
        let name_elem = $.newElement('div', null, 'sub_name');
        let name_text = sub.user_name;
        if (sub.user_id === current_user_id) {
            name_text += ' (Vous)';
        }
        name_elem.appendChild($.newTextNode(name_text));
        li.appendChild(name_elem);
        let rank_elem = $.newElement('div', null, 'sub_rank');
        const rank_text = sub.owner ? 'Gérant' : 'Membre';
        let rank_link = $.newElement('a', null, null, rank_text);
        rank_link.setAttribute('href', '#');
        rank_link.addEventListener('click', (ev) => {
            this.clickRank(sub);
            ev.preventDefault();
        });
        rank_elem.appendChild(rank_link);
        li.appendChild(rank_elem);
        let delete_icon = document.createElement('div');
        delete_icon.classList.add('delete_icon');
        delete_icon.appendChild(document.createTextNode('✕'));
        delete_icon.addEventListener('click', (ev) => {
            this.removeSubscription(i);
        });
        li.appendChild(delete_icon);
        return li;
    }
    rebuildSearchResults() {
        $.empty(this.search_result_list);
        const show_search_result_ui = (this.search_error != null
            || this.search_results != null);
        this.search_title.hidden = !show_search_result_ui;
        this.search_result_list.hidden = !show_search_result_ui;
        if (this.search_error != null) {
            let li = $.newElement('li');
            li.appendChild($.newElement('div', null, 'name', this.search_error));
            this.search_result_list.appendChild(li);
        }
        else if (this.search_results != null) {
            for (const result of this.search_results) {
                let li = $.newElement('li');
                let td1 = $.newElement('div', null, 'name');
                td1.appendChild($.newTextNode(result.user_name));
                li.appendChild(td1);
                if (!this.subscriptionExists(result.user_id)) {
                    let add_button = $.newElement('input');
                    add_button.setAttribute('type', 'button');
                    add_button.setAttribute('value', 'Ajouter');
                    add_button.addEventListener('click', (ev) => {
                        this.addSubscription(result.user_id, result.user_name);
                    });
                    li.appendChild(add_button);
                }
                else {
                    let text = 'Déja ajouté';
                    if (current_user_id === result.user_id) {
                        text = "C'est vous !";
                    }
                    const label_cant_add = $.newElement('div', null, null, text);
                    li.appendChild(label_cant_add);
                }
                this.search_result_list.appendChild(li);
            }
            if (this.search_results.length === 0) {
                let li = $.newElement('li');
                let td1 = $.newElement('div', null, 'name', 'Aucun utilisateur trouvé.');
                li.appendChild(td1);
                this.search_result_list.appendChild(li);
            }
        }
        else { /* No results */ }
    }
    async startSearchPotentialMembers() {
        const query = this.search_bar.value;
        if (query === '') {
            return;
        }
        try {
            await this.search_query_manager.askQuery(query);
        }
        catch (err) {
            console.error(err);
            this.search_error = "Le serveur n'a pas répondu correctement, veuillez reéssayer. Erreur : " + err;
            this.rebuildSearchResults();
        }
    }
    async executeSearchPotentialMembers(query) {
        this.search_bar.classList.add('loading');
        let req;
        try {
            req = await ajax('/get/search_user_names', 'GET', { q: query });
        }
        finally {
            this.search_bar.classList.remove('loading');
        }
        return req;
    }
    treatSearchPotentialMembers(query, req) {
        let json_result;
        try {
            json_result = JSON.parse(req.responseText);
        }
        catch (err) {
            this.search_error = "Le serveur n'a pas répondu dans un format valide. Erreur : " + String(err);
            this.rebuildSearchResults();
            return;
        }
        if (json_result.status !== 'ok') {
            this.search_error = "Erreur : " + json_result.msg;
        }
        else {
            this.search_error = null;
            this.search_results = json_result.result.map(([id, name]) => {
                return { user_id: id, user_name: name };
            });
        }
        this.rebuildSearchResults();
    }
    subscriptionExists(user_id) {
        for (const sub of this.board.subs) {
            if (sub.user_id === user_id) {
                return true;
            }
        }
        return false;
    }
    addSubscription(user_id, user_name) {
        this.board.subs.push({
            user_id: user_id,
            user_name: user_name,
            owner: false,
            brand_new: true,
        });
        this.rebuildSubs();
        this.rebuildSearchResults();
    }
    removeSubscription(i) {
        const subscription = this.board.subs[i];
        this.board.subs.splice(i, 1);
        if (!subscription.brand_new) {
            this.board.deleted_subs.push(subscription);
        }
        this.rebuildSubs();
        this.rebuildSearchResults();
    }
    clickRank(sub) {
        sub.owner = !sub.owner;
        this.rebuildSubs();
    }
}
class QueryManager {
    constructor(query_function, use_results) {
        this.query_function = query_function;
        this.use_results = use_results;
        this.query_ongoing = false;
        this.last_query_done = null;
        this.query_needed = null;
    }
    /*
        Appelle la fonction asynchrone `query_function`, Si il y a eu un nouvel
        appel à askQuery entre temps, réappelle `query_function`
        Puis donne le résultat à `use_results`.
    */
    async askQuery(query) {
        if (this.query_ongoing) {
            this.query_needed = query;
            return;
        }
        let result;
        while (true) {
            if (this.last_query_done != null && query === this.last_query_done) {
                return;
            }
            this.query_ongoing = true;
            try {
                result = await this.query_function(query);
            }
            finally {
                this.query_ongoing = false;
            }
            if (this.query_needed != null && this.query_needed !== query) {
                query = this.query_needed;
                this.query_needed = null;
            }
            else {
                break;
            }
        }
        this.last_query_done = query;
        this.use_results(query, result);
    }
}
