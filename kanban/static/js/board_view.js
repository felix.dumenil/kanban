/// <reference path="common.ts"/>
/// <reference path="task_editor.ts"/>
"use strict";
const drag_task_mime = 'application/x-kanban.drag_task';
var board_view;
(function (board_view) {
    let TaskColor;
    (function (TaskColor) {
        TaskColor["white"] = "white";
        TaskColor["red"] = "red";
        TaskColor["orange"] = "orange";
        TaskColor["yellow"] = "yellow";
        TaskColor["green"] = "green";
        TaskColor["blue"] = "blue";
        TaskColor["purple"] = "purple";
        TaskColor["pink"] = "pink";
        TaskColor["brown"] = "brown";
        TaskColor["grey"] = "grey";
    })(TaskColor = board_view.TaskColor || (board_view.TaskColor = {}));
    board_view.color_order = [
        TaskColor.white,
        TaskColor.red,
        TaskColor.orange,
        TaskColor.yellow,
        TaskColor.green,
        TaskColor.blue,
        TaskColor.purple,
        TaskColor.pink,
        TaskColor.brown,
        TaskColor.grey,
    ];
    function canMoveOrDeleteTask(task) {
        if (current_user_is_owner) {
            return true;
        }
        else if (task.assigned_user_id == null) {
            return true;
        }
        else if (task.assigned_user_id === current_user_id) {
            return true;
        }
        return false;
    }
    board_view.canMoveOrDeleteTask = canMoveOrDeleteTask;
})(board_view || (board_view = {}));
const task_sorters = {
    description: {
        display_name: 'Description',
        tooltip: "Trie par l'ordre alphabétique du contenu de la tâche.",
        sort_criteria: (a, b) => {
            // FLAW : the sort is done over the BBCode, when it should be done
            // over the plain-text version.
            return a.description.localeCompare(b.description);
        },
    }, due_date: {
        display_name: 'Date de rendu',
        tooltip: "Trie par la date de rendu, les tâches plus urgentes sont en premier.",
        sort_criteria: (a, b) => {
            // the most urgent tasks come first
            if (a.due_date == null) {
                return b.due_date == null ? 0 : 1;
            }
            else if (b.due_date == null) {
                return -1;
            }
            return a.due_date.getTime() - b.due_date.getTime();
        },
    }, color: {
        display_name: 'Couleur',
        tooltip: "Trie par la couleur.",
        sort_criteria: (a, b) => {
            return board_view.color_order.indexOf(a.color)
                - board_view.color_order.indexOf(b.color);
        },
    }, assigned_user: {
        display_name: 'Affectée à',
        tooltip: "Trie par l'utilisateur affecté à la tâche.",
        sort_criteria: (a, b) => {
            // Put unassigned tasks at the bottom
            const a_is_unassigned = a.assigned_user_id == null;
            const b_is_unassigned = b.assigned_user_id == null;
            if (a_is_unassigned != b_is_unassigned) {
                return a_is_unassigned ? 1 : -1;
            }
            else if (a_is_unassigned && b_is_unassigned) {
                return 0;
            }
            const a_is_mine = a.assigned_user_id === current_user_id;
            const b_is_mine = b.assigned_user_id === current_user_id;
            if (a_is_mine != b_is_mine) {
                return a_is_mine ? -1 : 1;
            }
            return a.assigned_user_id - b.assigned_user_id;
        },
    }, urgency: {
        display_name: 'Urgence',
        tooltip: "Trie par la date, puis par l'utilisateur affecté. "
            + "Les tâches pour d'autres utilisateurs sont en dernier.",
        sort_criteria: (a, b) => {
            /* We want task assigned to me or to no-one first, then
               tasks for other people */
            const a_is_mine = a.assigned_user_id === current_user_id;
            const b_is_mine = b.assigned_user_id === current_user_id;
            const a_is_unassigned = a.assigned_user_id == null;
            const b_is_unassigned = b.assigned_user_id == null;
            const a_is_others = !(a_is_mine || a_is_unassigned);
            const b_is_others = !(b_is_mine || b_is_unassigned);
            if (a_is_others != b_is_others) {
                return a_is_others ? 1 : -1;
            }
            if (a_is_others) { // && b_is_others
                return task_sorters.due_date.sort_criteria(a, b);
            }
            else {
                // First sort by due date
                const due_date_sort = task_sorters.due_date.sort_criteria(a, b);
                if (due_date_sort !== 0) {
                    return due_date_sort;
                }
                // For two tasks due the same date, put first the one assigned to me
                if (a_is_mine != b_is_mine) {
                    return a_is_mine ? -1 : 1;
                }
            }
            return 0;
        },
    }, relevance: {
        display_name: 'Pertinence',
        tooltip: "Place d'abord les tâches affectées à vous, puis à personne, "
            + "puis à d'autres utilisateurs. "
            + "Les tâches sont ensuite triées par date de rendu.",
        sort_criteria: (a, b) => {
            /* Put tasks assigned to me, then to no-one, then to others */
            const a_is_mine = a.assigned_user_id === current_user_id;
            const b_is_mine = b.assigned_user_id === current_user_id;
            if (a_is_mine != b_is_mine) {
                return a_is_mine ? -1 : 1;
            }
            const a_is_unassigned = a.assigned_user_id == null;
            const b_is_unassigned = b.assigned_user_id == null;
            if (a_is_unassigned != b_is_unassigned) {
                return a_is_unassigned ? -1 : 1;
            }
            /* Then sort by due date */
            return task_sorters.due_date.sort_criteria(a, b);
        },
    },
};
class BoardViewer {
    constructor(board, container) {
        this.board = {
            name: board.name,
            public: board.public,
            columns: board.columns.map((col) => {
                return {
                    id: col.id,
                    name: col.name,
                    mandatory: col.mandatory,
                    tasks: col.tasks.map((task) => {
                        return {
                            id: task.id,
                            column_id: col.id,
                            description: task.description,
                            color: task.color,
                            assigned_user_id: task.assigned_user_id,
                            due_date: task.due_date != null ? new Date(task.due_date) : null,
                        };
                    }),
                };
            }),
            subs: board.subs,
        };
        this.column_views = [];
        this.container = container;
        this.task_drags = new Map();
        this.task_drag_num = 0;
        this.sub_names = new Map();
        for (const sub of this.board.subs) {
            this.sub_names.set(sub.user_id, sub.user_name);
        }
        $.empty(this.container);
        for (const column of this.board.columns) {
            const column_view = new ColumnView(column, this.sub_names);
            this.column_views.push(column_view);
            this.container.appendChild(column_view.container);
        }
    }
    editedTask(edited_task) {
        // find column
        let found_column_view = null;
        for (const column_view of this.column_views) {
            if (column_view.column.id === edited_task.column_id) {
                found_column_view = column_view;
                break;
            }
        }
        if (found_column_view == null)
            throw "couldn't find column id " + String(edited_task.column_id);
        // find task
        if (edited_task.id == null)
            throw "edited_task should have an id" + JSON.stringify(edited_task);
        for (let i = 0; i < found_column_view.column.tasks.length; i++) {
            if (edited_task.id === found_column_view.column.tasks[i].id) {
                found_column_view.column.tasks[i] = edited_task;
                found_column_view.sortTasks();
                found_column_view.rebuildTaskList();
                return;
            }
        }
        // it's a new task
        found_column_view.column.tasks.push(edited_task);
        found_column_view.sortTasks();
        found_column_view.rebuildTaskList();
    }
    deletedTask(column_id, task_id) {
        // find column
        let found_column_view = null;
        for (const column_view of this.column_views) {
            if (column_view.column.id === column_id) {
                found_column_view = column_view;
                break;
            }
        }
        if (found_column_view == null)
            throw "couldn't find column id " + String(column_id);
        for (let i = 0; i < found_column_view.column.tasks.length; i++) {
            if (task_id === found_column_view.column.tasks[i].id) {
                found_column_view.column.tasks.splice(i, 1);
                found_column_view.rebuildTaskList();
                return;
            }
        }
        throw "couldn't find task id " + String(task_id);
    }
    addTaskDrag(task_view) {
        let column_view = null;
        for (const col_view of this.column_views) {
            if (col_view.column.id === task_view.column_id) {
                column_view = col_view;
                break;
            }
        }
        if (column_view == null)
            throw Error("unreachable");
        const task_drag_num = this.task_drag_num;
        this.task_drag_num++;
        const task_drag = {
            task_view: task_view,
            from_column_view: column_view,
        };
        this.task_drags.set(task_drag_num, task_drag);
        return task_drag_num;
    }
    getSubNameFromId(user_id) {
        for (const sub of this.board.subs) {
            if (sub.user_id === user_id) {
                return sub.user_name;
            }
        }
        return "[User " + String(user_id) + "]";
    }
}
class ColumnView {
    constructor(column, sub_names) {
        this.column = column;
        this.sub_names = sub_names;
        if (read_only) {
            this.current_sort = task_sorters.due_date;
        }
        else {
            this.current_sort = task_sorters.relevance;
        }
        this.sortTasks();
        this.task_views = [];
        this.container = $.newElement('div', null, 'board-column');
        { // header
            let header = $.newElement('header');
            let name = $.newElement('h3');
            name.appendChild($.newTextNode(this.column.name));
            // in case the name is cut off, give a tooltip
            name.setAttribute('title', this.column.name);
            header.appendChild(name);
            let header_actions = $.newElement('div', null, 'header-actions');
            header.appendChild(header_actions);
            if (!read_only) {
                let add_task_button = $.newElement('a', null, 'add-task-button');
                add_task_button.setAttribute('href', '#');
                add_task_button.appendChild($.newTextNode('+'));
                add_task_button.addEventListener('click', (ev) => this.clickAddTask());
                header_actions.appendChild(add_task_button);
            }
            let task_sort_select = $.newElement('select', null, 'task-sort');
            task_sort_select.setAttribute('title', this.current_sort.tooltip);
            for (const k in task_sorters) {
                const task_sorter = task_sorters[k];
                let option = $.newElement('option', null, null, task_sorter.display_name);
                option.setAttribute('value', k);
                if (task_sorter === this.current_sort) {
                    option.setAttribute('selected', 'selected');
                }
                task_sort_select.appendChild(option);
            }
            task_sort_select.addEventListener('change', (ev) => {
                const option = ev.target;
                this.current_sort = task_sorters[option.value];
                task_sort_select.setAttribute('title', this.current_sort.tooltip);
                this.sortTasks();
                this.rebuildTaskList();
            });
            header_actions.appendChild(task_sort_select);
            this.container.appendChild(header);
        }
        // task list
        this.task_list = $.newElement('div', null, 'task-list');
        this.rebuildTaskList();
        this.container.appendChild(this.task_list);
        this.task_list.addEventListener('dragover', (ev) => {
            // note: we can't use getData here, since chrome doesn't allow it.
            ev.preventDefault();
            ev.dataTransfer.dropEffect = 'move';
        });
        this.task_list.addEventListener('drop', (ev) => {
            const task_drag_index_data = ev.dataTransfer.getData(drag_task_mime);
            if (task_drag_index_data === "")
                return;
            const task_drag_index = Number(task_drag_index_data);
            const task_drag = board_viewer.task_drags.get(task_drag_index);
            if (task_drag == null)
                throw Error("unreachable");
            ev.preventDefault();
            this.dropTask(task_drag);
            board_viewer.task_drags.delete(task_drag_index);
        });
    }
    clickAddTask() {
        task_editor.openTask(null, this.column.id);
    }
    sortTasks() {
        this.column.tasks.sort((a, b) => {
            const comp = this.current_sort.sort_criteria(a, b);
            if (comp !== 0)
                return comp;
            return a.id - b.id;
        });
    }
    rebuildTaskList() {
        this.task_views = [];
        $.empty(this.task_list);
        for (const task of this.column.tasks) {
            const task_view = new TaskView(task, this.column.id, this.sub_names);
            this.task_views.push(task_view);
            this.task_list.appendChild(task_view.container);
        }
    }
    async dropTask(task_drag) {
        const task_id = task_drag.task_view.task.id;
        const from_column_id = task_drag.task_view.column_id;
        const to_column_id = this.column.id;
        if (from_column_id === to_column_id)
            return;
        let new_task = {
            id: task_id,
            column_id: to_column_id,
            description: task_drag.task_view.task.description,
            color: task_drag.task_view.task.color,
            assigned_user_id: task_drag.task_view.task.assigned_user_id,
            due_date: task_drag.task_view.task.due_date,
        };
        let req;
        try {
            req = await ajaxSendJson(script_root + '/post/edit_task', {
                task: task_editor.prepareTaskForSubmission(new_task)
            });
        }
        catch (e) {
            console.error(e);
            alert("Impossible de déplacer la tâche.");
            return;
        }
        let response;
        try {
            response = JSON.parse(req.responseText);
        }
        catch (e) {
            alert('Impossible de déplacer la tâche : ' + e);
            return;
        }
        if (response.status !== 'ok') {
            alert('Impossible de déplacer la tâche : ' + response.msg);
            return;
        }
        else {
            task_drag.from_column_view.removeTask(task_drag.task_view);
            this.column.tasks.push(new_task);
            this.sortTasks();
            this.rebuildTaskList();
        }
    }
    removeTask(task_view) {
        const index = this.task_views.indexOf(task_view);
        if (index === -1)
            throw Error("unreachable");
        this.column.tasks.splice(index, 1);
        this.rebuildTaskList();
    }
}
class TaskView {
    constructor(task, column_id, sub_names) {
        this.task = task;
        this.column_id = column_id;
        this.container = $.newElement('div', 'task_' + String(task.id), ['task', 'task-' + task.color]);
        if (!read_only) {
            if (board_view.canMoveOrDeleteTask(this.task)) {
                this.container.setAttribute('draggable', 'true');
                this.container.addEventListener('dragstart', (ev) => {
                    ev.dataTransfer.dropEffect = "move";
                    const task_drag_index = board_viewer.addTaskDrag(this);
                    ev.dataTransfer.setData(drag_task_mime, String(task_drag_index));
                    this.container.classList.add('dragging');
                });
                this.container.addEventListener('dragend', (ev) => {
                    const task_drag_index = Number(ev.dataTransfer.getData(drag_task_mime));
                    board_viewer.task_drags.delete(task_drag_index);
                    this.container.classList.remove('dragging');
                });
            }
            this.container.addEventListener('click', (ev) => this.clickTask());
        }
        // Description
        let description_elem = $.newElement('div', null, 'description');
        description_elem.appendChild(parseBBCode(task.description));
        this.container.appendChild(description_elem);
        let misc_info_elem = $.newElement('div', null, 'misc-info');
        // Assigned user
        if (this.task.assigned_user_id != null) {
            let assigned_user_elem = $.newElement('div', null, 'assigned-user');
            const prefix_elem = $.newElement('div', null, 'icon-user');
            assigned_user_elem.appendChild(prefix_elem);
            let assigned_user_text;
            if (current_user_id === this.task.assigned_user_id) {
                assigned_user_text = 'Vous';
            }
            else {
                let assigned_user_name = sub_names.get(this.task.assigned_user_id);
                if (assigned_user_name == null) {
                    assigned_user_name = '[User' + this.task.assigned_user_id + ']';
                }
                assigned_user_text = assigned_user_name;
            }
            assigned_user_elem.appendChild($.newTextNode(assigned_user_text));
            misc_info_elem.appendChild(assigned_user_elem);
        }
        // Due date
        if (this.task.due_date != null) {
            const due_date_str = this.task.due_date.toLocaleDateString();
            const due_date_elem = $.newElement('date', null, null, due_date_str);
            const prefix_elem = $.newElement('div', null, 'icon-date');
            const due_date_container = $.newElement('div', null, 'due-date', [prefix_elem, due_date_elem]);
            misc_info_elem.appendChild(due_date_container);
        }
        if (misc_info_elem.hasChildNodes()) {
            this.container.appendChild(misc_info_elem);
        }
    }
    clickTask() {
        task_editor.openTask(this.task, this.column_id);
    }
}
