import os
import flask
from flask import request
import hashlib
import argon2

from . import model, db, ph

bp = flask.Blueprint('auth', __name__)


@bp.route('/signup')
def signup():
    if get_current_user() is not None:
        flask.flash("Vous êtes déja connecté.", 'error')
        return flask.redirect(flask.url_for('homepage.homepage'))

    return flask.render_template(
        'signup.html',
        current_user=get_current_user(),
    )


@bp.route('/post/connect', methods=['POST'])
def do_connect():
    if get_current_user() is not None:
        flask.flash("Vous êtes déja connecté.", 'error')
        return flask.redirect(flask.url_for('auth.signup'))

    if ('login' not in request.form or
            'password' not in request.form):
        flask.flash("Requête incorrecte.", 'error')
        return flask.redirect(flask.url_for('auth.signup'))

    user = model.User.get_from_login_password(
        request.form['login'], request.form['password'])

    if user is None:
        flask.flash("Identifiant ou mot de passe incorrect", 'error')
        return flask.redirect(flask.url_for('homepage.homepage'))

    flask.session['user_id'] = user.id

    return flask.redirect(flask.url_for('homepage.homepage'))


@bp.route('/post/signup', methods=['POST'])
def do_signup():
    if get_current_user() is not None:
        flask.flash("Vous êtes déja connecté", 'error')
        return flask.redirect(flask.url_for('homepage.homepage'))

    if request.form.get('login', '') == '':
        flask.flash("Nom d'utilisateur obligatoire", 'error')
        return flask.redirect(flask.url_for('auth.signup'))
    elif request.form.get('display_name', '') == '':
        flask.flash("Nom d'affichage obligatoire", 'error')
        return flask.redirect(flask.url_for('auth.signup'))
    elif request.form.get('password', '') == '':
        flask.flash("Mot de passe obligatoire", 'error')
        return flask.redirect(flask.url_for('auth.signup'))
    elif request.form.get('password_redundancy', '') == '':
        flask.flash("Mot de passe obligatoire", 'error')
        return flask.redirect(flask.url_for('auth.signup'))

    if request.form['password'] != request.form['password_redundancy']:
        flask.flash("Les deux mots de passe ne sont pas identiques", 'error')
        return flask.redirect(flask.url_for('auth.signup'))

    existing_user = model.User.get_from_login(request.form['login'])
    if existing_user is not None:
        flask.flash("Un utilisateur existe déja avec ce nom", 'error')
        return flask.redirect(flask.url_for('auth.signup'))

    new_user = model.User.create(
        request.form['login'],
        request.form['display_name'],
        request.form['password']
    )

    db.session.add(new_user)
    db.session.commit()
    flask.session['user_id'] = new_user.id

    return flask.redirect(flask.url_for('homepage.homepage'))


@bp.route('/logout')
def logout():
    if 'user_id' in flask.session:
        del flask.session['user_id']
        flask.flash("Vous êtes maintenant déconnecté")
    else:
        flask.flash("Vous êtes déja déconnecté !", "error")
    return flask.redirect(flask.url_for('homepage.homepage'))


def get_current_user():
    if 'user_id' in flask.session:
        return model.User.query.get(flask.session['user_id'])
    else:
        return None
