import datetime
from typing import Optional
import flask
from flask import request

from .auth import get_current_user
from . import model, db, apply_csp_with_nonce
from .json_validation import validate_json_with_desc

bp = flask.Blueprint('board_view', __name__)


@bp.route('/board/<int:board_id>_<string:board_name>')
@bp.route('/board/<int:board_id>', defaults={'board_name': None})
@apply_csp_with_nonce
def board_view(board_id: int, board_name: Optional[str], csp_nonce: str):
    current_user = get_current_user()
    board = model.Board.query \
        .options(db.selectinload(model.Board.columns, model.Column.tasks)) \
        .get(board_id)

    if board is None:
        return flask.render_template(
            'error_page.html',
            current_user=current_user,
            err_msg="Ce tableau n'existe pas.",
        ), 404

    if not board.can_be_viewed_by(current_user):
        return flask.render_template(
            'error_page.html',
            current_user=current_user,
            err_msg="Vous n'avez pas le droit de voir ce tableau.",
        ), 403

    if board.get_urlified_name() != board_name:
        return flask.redirect(board.get_url())

    board_json = {
        'id': board.id,
        'name': board.name,
        'public': board.public,
        'columns': [{
            'id': col.id,
            'name': col.name,
            'mandatory': col.mandatory,
            'tasks': [{
                'id': task.id,
                'description': task.description,
                'color': task.color.name,
                'assigned_user_id': task.assigned_user_id,
                'due_date': (
                    None
                    if task.due_date is None
                    else task.due_date.isoformat()),
            } for task in col.tasks],
        } for col in board.columns],
        'subs': [{
            'user_id': sub.user_id,
            'user_name': sub.user.display_name,
        } for sub in board.subs],
    }

    return flask.render_template(
        'board_view.html',
        current_user=current_user,
        board=board,
        board_json=board_json,
        read_only=not board.can_be_edited_by(current_user),
        csp_nonce=csp_nonce,
    )


@bp.route('/post/edit_task', methods=['POST'])
def do_edit_task():
    def error(msg: str):
        return flask.jsonify(status='err', msg=msg)

    validation_model = {'task': {
        'id': ('int', 'null'),
        'column_id': 'int',
        'description': 'str',
        'color': tuple(['val:' + c.name for c in model.TaskColor]),
        'assigned_user_id': ('int', 'null'),
        'due_date': ('str', 'null'),
    }}
    validation_err = validate_json_with_desc(request.json, validation_model)

    if validation_err is not None:
        flask.current_app.logger.debug(validation_err)
        return error("Requête incomplète"), 400

    task_json = request.json['task']

    current_user = get_current_user()

    # get column
    column = model.Column.query.get(task_json['column_id'])
    if column is None:
        return error("Cette colonne n'existe pas"), 400

    # get board
    board = column.board
    if not board.can_be_edited_by(current_user):
        return error("Vous ne pouvez pas modifier ce tableau"), 403

    # get task
    if task_json['id'] is not None:
        task = model.Task.query.get(task_json['id'])
    else:
        task = model.Task()

    if not task.can_be_moved_to(current_user, column):
        return error(
            "Vous ne pouvez pas déplacer la tâche vers cette colonne"
        ), 400

    # get the assigned user
    if task_json['assigned_user_id'] is None:
        assigned_user = None
    else:
        assigned_user = model.User.query.get(task_json['assigned_user_id'])
        if assigned_user is None:
            return error("Cet utilisateur affecté n'existe pas"), 400

    if not board.can_assign_user(
            current_user,
            assigned_user,
            task.assigned_user):
        return error("Vous ne pouvez pas affecter cet utilisateur"), 403

    # update the task
    task.description = task_json['description']
    task.assigned_user_id = task_json['assigned_user_id']
    task.color = model.TaskColor[task_json['color']]

    if task_json['due_date'] is None:
        task.due_date = None
    else:
        # date.fromisoformat is python3.7+ only :(
        try:
            task.due_date = datetime.datetime.strptime(
                task_json['due_date'],
                "%Y-%m-%d",
            )
        except ValueError:
            return error("Date invalide"), 400

    task.column = column

    db.session.add(task)
    db.session.commit()

    # import time
    # time.sleep(3.0)

    return flask.jsonify(status='ok', task_id=task.id)


@bp.route('/post/delete_task', methods=['POST'])
def do_delete_task():
    def error(msg: str):
        return flask.jsonify(status='err', msg=msg)

    validation_model = {'task_id': 'int'}
    validation_err = validate_json_with_desc(request.json, validation_model)

    if validation_err is not None:
        flask.current_app.logger.debug(validation_err)
        return error("Requête incomplète."), 400

    task = model.Task.query.get(request.json['task_id'])
    if task is None:
        return error("Cette tâche n'existe pas"), 404

    current_user = get_current_user()

    if not task.can_be_deleted_by(current_user):
        return error("Vous ne pouvez pas supprimer cette tâche"), 403

    db.session.delete(task)
    db.session.commit()

    # import time
    # time.sleep(3.0)

    return flask.jsonify(status='ok')
